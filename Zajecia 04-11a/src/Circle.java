public class Circle extends Point2{

    double r;

    public Circle(){
        super();
        setType(FigureType.CIRCLE);
        r=0.0;
    }

    public Circle(double x, double y, double r){
        super(x,y);
        setType(FigureType.CIRCLE);
        this.r=r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public String toString(){
        return super.toString() + " radius: "+r;
    }

    public boolean equals(Circle circle){
        boolean result=super.equals(circle);
        return(result && r == circle.getR());
    }

    public double getArea(){
        return Math.PI*r*r;
    }

    public double getCircuit(){
        return 2*Math.PI*r;
    }
}
