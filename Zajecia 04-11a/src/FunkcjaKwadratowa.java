public class FunkcjaKwadratowa {

    private int a;
    private int b;
    private int c;

    public FunkcjaKwadratowa(){
        a=0;
        b=0;
        c=0;
    }

    public FunkcjaKwadratowa(int a, int b, int c){
        this.a=a;
        this.b=b;
        this.c=c;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public double getResult(int x){
        return ((getA()*Math.pow(x,2))+(getB()*x)+getC());
    }

    public double getDelta(){
        return (Math.pow(getB(),2)-(4*getA()*getC()));
    }

    public Point getPeak(){
        double delta=getDelta();
        if(delta>0){
            return new Point(-getB()/(2*getA()), (int) ((-delta)/(4*getA())));
        }
        else{
            return null;
        }
    }

    public Point[] getZeroPlaces(){
        double delta=getDelta();
        if(delta>0){
            Point[] points=new Point[2];
            points[0]=new Point((int) ((-b-Math.sqrt(delta))/(2*getA())),0);
            points[1]=new Point((int) ((-b+Math.sqrt(delta))/(2*getA())),0);

            return points;
        } else if(delta==0){
            Point[] points=new Point[1];
            points[0]=new Point((-getB()/(2*getA())), (int) 0.0);

            return points;
        } else{
            return null;
        }
    }

    public String toString(){
        return "f(x)= "+getA()+" * x^2 + "+getB()+" * x + "+getC();
    }

    //TESTING

    public static void main(String[] args) {
        FunkcjaKwadratowa f=new FunkcjaKwadratowa();

        double result;
        f.setA(2);
        f.setB(-2);
        f.setC(10);

        result=f.getResult(5);
        System.out.println(result);

        System.out.println();

        System.out.println(f);
        System.out.println(f.getDelta());
        System.out.println(f.getPeak());
        Point[] pts=f.getZeroPlaces();
        if(pts!=null){
            for (int i = 0; i <pts.length ; i++) {
                System.out.println(pts[i]);
            }
        }
        else{
            System.out.println("Function "+f+" does not touch/cross OX..");
        }
    }




}
