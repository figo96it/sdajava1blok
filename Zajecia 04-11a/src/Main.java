public class Main {

    public static void main(String[] args) {
        /*Figure fig=new Figure();
        System.out.println(fig);

        Point2 p=new Point2();
        System.out.println(p);

        System.out.println();
        System.out.println(fig.equals(p));
        System.out.println(p.equals(fig));
        System.out.println(p.equals(p));

        Point2 p2=new Point2(3,4);
        System.out.println(p.equals(p2));

        Circle c=new Circle(3,4,0);
        Circle c2=new Circle(3,4,5);
        System.out.println(c);
        System.out.println(c2);
        System.out.println(c.equals(c2));
        System.out.println(p2.equals(c));

        System.out.println();

        Line l1=new Line(new Point2(2,9), new Point2(2,4));
        Line l2=new Line(new Point2(2,9), new Point2(3,4));
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l1.equals(l2));
        System.out.println(l2.equals(l2));

        Polygon polygon=new Polygon();
        System.out.println(polygon);
        polygon.addEdge(new Point2(2,12));
        polygon.addEdge(new Point2(-5,6));
        polygon.addEdge(new Point2(4,-1));
        polygon.addEdge(new Point2(2,5));
        polygon.addEdge(new Point2(3,8));
        System.out.println(polygon);
        polygon.removeEdge(4);
        System.out.println(polygon);
        polygon.setEdge(1,new Point2(-10,-10));
        System.out.println(polygon);
        */

        Figure[] figures=new Figure[5];
        figures[0]=new Figure();
        figures[1]=new Point2(2,5);
        figures[2]=new Circle(2,5,5);
        figures[3]=new Line((Point2)figures[1],new Point2(2,4));
        figures[4]=new Polygon();
        ((Polygon)figures[4]).addEdge(new Point2(2,4));
        ((Polygon)figures[4]).addEdge(new Point2(-5,6));
        ((Polygon)figures[4]).addEdge(new Point2(4,-1));

        for(int i=0;i<figures.length;i++){
            System.out.println(figures[i]);
        }

    }
}
