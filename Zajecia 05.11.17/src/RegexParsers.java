import java.util.Scanner;

public class RegexParsers {

    public static boolean checkColor(String arg){
        String regexColor="Red|Green|Blue|Black|White";
        return arg.matches(regexColor) || arg.matches(regexColor.toUpperCase()) || arg.matches(regexColor.toLowerCase());
    }

    public static boolean checkInteger(String arg){
        String regexInteger= "[-+]?[0-9]+";
        return arg.matches(regexInteger);
    }

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        String s=cin.nextLine();
        do {
            System.out.println("Wprowadz kolor: ");
            if (checkColor(s)) {
                System.out.println("Poprawnie. Dziekuje.");
            } else {
                System.out.println("Niepoprawnie. Spróbuj jeszcze raz.");
            }
        }while(!s.equals(""));
    }
}
