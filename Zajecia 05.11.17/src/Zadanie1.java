import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Cześć. Wprowadź date urodzenia (dd/MM.yyyy) !");
        String toParse=cin.nextLine();

        LocalDate birthday=LocalDate.parse(toParse,formatter);
        System.out.println(birthday);

        LocalDate now=LocalDate.now();

        now=now.minusYears((birthday.getYear()));
        now=now.minusMonths(birthday.getMonthValue());
        now=now.minusDays(birthday.getDayOfMonth());

        System.out.println("Żyjesz już: ");
        System.out.println(" - "+now.getYear()+" lat.");
        System.out.println(" - "+now.getMonthValue()+" miesięcy.");
        System.out.println(" - "+now.getDayOfMonth()+" dni.");


    }
}
