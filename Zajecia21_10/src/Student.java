public class Student {

    public Student(){
        firstName="Dariusz";
        lastName="Figinski";
        age=47;

        counter++;
    }

    public Student(String first, String last){
        firstName= first;
        lastName= last;

        counter++;
    }

    public Student(String first, String last,int x){
        firstName= first;
        lastName= last;
        age=x;

        counter++;
    }

    public Student(Student s){
        firstName= s.firstName;
        lastName= s.lastName;
        age=s.age;

        counter++;

    }




    private String firstName;
    private String lastName;
    private int age;

    public static int counter=0;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString(){
        String x="Dane są niezidentyfikowane.";
        if(firstName.equals("")){
            return x;
        }
        if(lastName.equals("")){
            return x;
        }
        if(age<0){
            return x;
        }
        String resoult= firstName+" "+lastName+ " ";
        if(age>0){
            resoult=resoult + ", lat: "+age;
        }
        else{
            resoult=resoult + "<Dane są niekompletne.>jn";
        }
        return resoult;
    }

    public static void main(String[] args) {

        Student s1,s2,s3,s4,s5;

        s1=new Student();
        s2=new Student();
        s3=new Student();
        s4=new Student("Damian","Figinski");
        s5=new Student();


        s1.setFirstName("Lukasz");
        s1.setLastName("A.");
        s1.setAge(20);

        s3.setFirstName("Lukasz");
        s3.setLastName("Z.");
        s3.setAge(34);

        s5=new Student(s1);


        System.out.println(s1.toString());
        System.out.println(s2.toString());
        System.out.println(s5.toString());
        System.out.println();

    }
}
