public class Przyklad {

    public static final int MIN=-5;
    public static final int MAX=5;

    public static void main(String[] args) {
        System.out.println(suma(12,30));
        System.out.println(suma(-3,22));
        System.out.println(suma(2,12));
        System.out.println(suma(-22,6));

    }

    private static double ogranicz(double a){
        if(a>MAX){
            a=a-(MAX-MIN);
        }

        if(a<MIN){
            a=a+(MAX-MIN);
        }

        return a;
    }

    public static double suma(double a, double b){
        return ogranicz(a+b);
    }

    public static double srednia(double a, double b){
        return ogranicz(suma(a,b)/2.0);
    }

    public static double minimum(double a,double b){
        if (a <= b) {
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }

    public static double maximum(double a,double b){
        if(a>=b){
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }
}
