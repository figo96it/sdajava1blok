import static java.lang.Math.pow;
import static java.lang.StrictMath.sqrt;

public class Point {

    private int wspx;
    private int wspy;


    public Point(){
        wspx=4;
        wspy=7;
    }

    public Point(int x,int y){
        wspx=x;
        wspy=y;
    }


    public int getWspx() {
        return wspx;
    }

    public int getWspy() {
        return wspy;
    }

    public void setWspx(int wspx) {
        this.wspx = wspx;
    }

    public void setWspy(int wspy) {
        this.wspy = wspy;
    }

    public String toString(){
        return "Punkt: [ "+wspx+", "+wspy+" ]";
    }

    public boolean equals(Point p){
        return ((wspx==p.wspx)&&(wspy==p.wspy));
    }

    public double odleglosc(Point p){
        double d =0.0;
        d=sqrt(pow((wspx-p.wspx),2)+pow((wspy-p.wspy),2));

        return d;
    }

    public void wektor(Point p){
        int s,k;
        s=(wspx-p.wspx);
        k=(wspy-p.wspy);

        System.out.println("Wektor V=[ "+s+", "+k+" ]");
    }

    public int cwiartka(Point p){
        int a=0;
        if((wspx>=0)&&(wspy>=0)){
            a=1;
        }
        else if((wspx<0)&&(wspy>=0)){
            a=2;
        }
        else if((wspx<0)&&(wspy<0)){
            a=3;
        }
        else if((wspx>0)&&(wspy<0)){
            a=4;
        }
        return a;
    }

    public static void main(String[] args) {
        Point p1,p2,p3,p4;

        p1=new Point();
        p2=new Point();
        p3=new Point();
        p4=new Point(2,1);

        p1.setWspx(3);
        p1.setWspy(9);

        System.out.println(p1.toString());
        System.out.println(p4.toString());
        System.out.println(p3.toString());








    }
}

