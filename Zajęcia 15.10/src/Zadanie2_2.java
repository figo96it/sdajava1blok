import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args) {

        int[] tablica=new int[20];

        Scanner wejscie=new Scanner(System.in);

        for(int i=0;i<tablica.length;i++) {
            if (i < 2) {
                System.out.println("Wprowadz " + (i+1) + " liczbe: ");
                tablica[i] = wejscie.nextInt();
            }
            else {
                tablica[i] = tablica[i - 2] + tablica[i - 1];
            }
        }

        for(int i=0;i<tablica.length;i++){
            System.out.println(tablica[i]);
        }

    }
}
