import java.util.Random;

public class Zadanie2_1 {
    public static void main(String[] args) {

        Random generator= new Random();

        int[] tablica=new int[10];
        int a;

        for(int i=0;i<10;i++){
            tablica[i]=generator.nextInt(30);
        }

        System.out.println();
        System.out.println("Elementy tablicy: ");

        for(int i=0;i<10;i++){
            System.out.println(tablica[i]);
        }

        for(int i=0;i<5;i++){
            a=tablica[i];
            tablica[i]=tablica[9-i];
            tablica[9-i]=a;
        }
        System.out.println();
        System.out.println("Po zamianie!");

        for(int i=0;i<10;i++){
            System.out.println(tablica[i]);
        }
    }
}
