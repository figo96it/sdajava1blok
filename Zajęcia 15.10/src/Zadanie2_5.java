import java.util.Random;
import java.util.Scanner;

public class Zadanie2_5 {

    public static void main(String[] args) {
        Scanner wejscie = new Scanner(System.in);
        System.out.println("Ile chcesz wprowadzic wierszy do tablicy ?");
        int x = wejscie.nextInt();
        System.out.println("Ile chcesz wprowadzic kolumn do tablicy ?");
        int y = wejscie.nextInt();
        double[][] tab = new double[x][y];

        losuj(tab);
        wypisz(tab);
        System.out.println("Suma wynosi " + suma(tab));
        System.out.println("Srednia wynosi " + srednia(tab));
        System.out.println("Minumum wynosi " + min(tab));
        System.out.println("Maximum wynosu " + max(tab));

    }

    public static void pobierz(double[][] T) {

        System.out.println("Program sluzy do wypelniania tablicy danymi wprowadzonymi przez uzytkownika.");
        Scanner wejscie = new Scanner(System.in);

        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T.length; j++) {
                System.out.println("Wprowadz " + (i + 1) + " liczbę.");
                T[i][j] = wejscie.nextDouble();
            }
        }

    }

    public static void losuj(double[][] T) {
        System.out.println("Program sluzy do wypelniania tablicy danymi wygenerowanymi przez komputer.");
        Random gen = new Random();

        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T.length; j++) {
                T[i][j] = gen.nextDouble()*100;
            }
        }
    }

    public static void wypisz(double[][] T) {

        System.out.print("T=[");

        for (int i = 0; i < T.length; i++) {
            if(i>0){
                System.out.println();
            }

            for (int j = 0; j < T.length; j++) {
                System.out.print(T[i][j]);
                if (i <= T.length - 1) {
                    System.out.print(", ");
                }
            }
        }
        System.out.print("]");
        System.out.println();

    }

    public static double suma(double[][] T) {
        double suma = 0.0;
        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T.length; j++) {
                suma = suma + T[i][j];
            }
        }
        return suma;
    }

    public static double srednia(double[][] T) {
        return suma(T) / T.length;
    }

    public static double max(double[][] T) {
        double max = T[0][0];
        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T.length; j++) {
                if (T[i][j] > max) {
                    max = T[i][j];
                }
            }
        }
        return max;
    }

    public static double min(double[][] T) {
        double min = T[0][0];
        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T.length; j++) {
                if (T[i][j] < min) {
                    min = T[i][j];
                }
            }
        }
        return min;

    }
}

