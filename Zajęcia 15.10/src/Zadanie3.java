import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        int n;

        Scanner wejscie=new Scanner(System.in);
        System.out.println("Ile chcesz wprowadzić liczb ?");
        n=wejscie.nextInt();

        int[] tablica=new int[n];
        Random generator=new Random();

        for(int x=0;x<n;x++){
            tablica[x]=generator.nextInt(40);
        }
        System.out.println();
        System.out.println("Wprowadzone liczby");

        for(int x=0;x<n;x++){
            System.out.println(tablica[x]);
        }
    }
}
