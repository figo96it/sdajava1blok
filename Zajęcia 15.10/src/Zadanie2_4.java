import java.util.Random;
import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) {
        Scanner wejscie=new Scanner(System.in);
        System.out.println("Ile chcesz wprowadzic liczb do tablicy ?");
        int n=wejscie.nextInt();
        double[] tab=new double[n];

        losuj(tab);
        wypisz(tab);
        System.out.println("Suma wynosi " + suma(tab));
        System.out.println("Srednia wynosi " + srednia(tab));
        System.out.println("Minumum wynosi " + min(tab));
        System.out.println("Maximum wynosu " + max(tab));
    }

    public static void pobierz(double[] T){

        System.out.println("Program sluzy do wypelniania tablicy danymi wprowadzonymi przez uzytkownika.");
        Scanner wejscie=new Scanner(System.in);

        for(int i=0;i<T.length;i++){
            System.out.println("Wprowadz " + (i+1) + " liczbę.");
            T[i]=wejscie.nextDouble();
        }

    }

    public static void losuj(double[] T){
        System.out.println("Program sluzy do wypelniania tablicy danymi wygenerowanymi przez komputer.");
        Random gen=new Random();

        for(int i=0;i<T.length;i++){
            T[i]=gen.nextDouble();
        }
    }

    public static void wypisz(double[] T){
        System.out.print("T=[");
        for(int i=0;i<T.length;i++){
            System.out.print(T[i]);
            if(i<T.length-1){
                System.out.print(", ");
            }
        }
        System.out.print(" ]");
    }

    public static double suma(double[] T){
        double suma=0.0;
        for(int i=0;i<T.length;i++){
            suma=suma+T[i];
        }
        return suma;
    }

    public static double srednia(double[] T){
        return suma(T)/T.length;
    }

    public static double max(double[] T){
        double max=T[0];
        for(int i=0;i<T.length;i++){
            if(T[i]>max){
                max=T[i];
            }
        }
        return max;
    }

    public static double min(double[] T){
        double min=T[0];
        for(int i=0;i<T.length;i++){
            if(T[i]<=min){
                min=T[i];
            }
        }
        return min;
    }



}
