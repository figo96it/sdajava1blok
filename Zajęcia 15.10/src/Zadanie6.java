import java.util.Random;
import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {

        int liczba1,liczba2;
        double[] tablica=new double[10];

        Random generator=new Random();

        Scanner wejscie=new Scanner(System.in);
        System.out.println("Podaj liczbe z poczatku przedzialu: ");
        liczba1=wejscie.nextInt();
        System.out.println("Podaj liczbe z końca przedzialu: ");
        liczba2=wejscie.nextInt();

        tablica[0]=liczba1;
        tablica[9]=liczba2;

        for(int x=1;x<9;x++){
            tablica[x]=generator.nextDouble();
        }

        System.out.println();
        System.out.println("Zawartość tablicy: ");

        for(int x=0;x<10;x++){
            System.out.println(tablica[x]);
        }

    }
}
