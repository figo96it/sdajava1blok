import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args) {

        double a,b;

        System.out.println("Podaj 1 liczbe rzeczywista: ");
        Scanner wejscie=new Scanner(System.in);
        a=wejscie.nextDouble();

        System.out.println("Podaj 2 liczbe rzeczywista: ");
        b=wejscie.nextDouble();

        System.out.println();
        System.out.print("Suma wynosi: " + suma(a,b));
        System.out.println();
        System.out.print("Srednia wynosi: " + srednia(a,b));
        System.out.println();
        System.out.print("Min wynosi: " + min(a,b));
        System.out.println();
        System.out.print("Max wynosi: " + max(a,b));


    }

    public static double suma(double a,double b){
        double suma=a+b;
        return suma;
    }

    public static double srednia(double a,double b){
        double srednia=(a+b)/2;
        return srednia;
    }

    public static double max(double a,double b){
        if(a>=b){
            return a;
        }
        else{
            return b;
        }
    }

    public static double min(double a,double b){
        if(a<=b){
            return a;
        }        else{
            return b;
        }
    }
}