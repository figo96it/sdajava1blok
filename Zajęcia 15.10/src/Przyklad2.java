import java.util.Random;

public class Przyklad2 {
    public static void metoda_void(){
        System.out.println("Cześć jestem VOID!");
    }

    public static int metoda_int(){
        System.out.println("Czesc, jestem INT, zwracam losowa liczbe.");

        Random gen=new Random();
        //int wynik=gen.nextInt();
        //return wynik;

        return gen.nextInt();
    }

    public static int metoda_int(int zakres){
        System.out.println("Czesc, jestem INT, zwracam losowa liczbe.");

        Random gen=new Random();
        return gen.nextInt(zakres);
    }

    public static void main(String[] args) {
        metoda_void();
        int los=metoda_int();
        System.out.println("Wylosowane: " + los);
        los=metoda_int(150);
        System.out.println("Wylosowane: " + los);
    }
}
