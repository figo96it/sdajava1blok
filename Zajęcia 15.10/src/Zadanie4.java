import java.util.Random;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        int n, min=0, max=0, suma=0, npar=0, par=0;

        Scanner wejscie=new Scanner(System.in);
        System.out.println("Ile chcesz wprowadzić liczb ?");
        n=wejscie.nextInt();

        int[] tablica=new int[n];
        Random generator=new Random();


        for(int x=0;x<n;x++){
            tablica[x]=generator.nextInt(40);
        }
        System.out.println();
        System.out.println("Wprowadzone liczby");

        min=tablica[0];
        max=tablica[0];

        for(int x=0;x<n;x++){
            System.out.println(tablica[x]);
            if(tablica[x]>max){
                max=tablica[x];
            }
            else if(tablica[x]<min){
                min=tablica[x];
            }
            suma=suma+tablica[x];

            if(((tablica[x])%2)==0){
                par=par+1;
            }
            else{
                npar=npar+1;
            }
        }

        System.out.println("Suma wylosowanych liczb wynosi " + suma +".");
        System.out.println("Jest " + par + " liczb parzystych");
        System.out.println("oraz " + npar + " licb nieparzystych.");
        System.out.println("Maksimum wylosowanych liczb to " + max);
        System.out.println("Minimum wylosowanych liczb to " + min);
    }
}
