import java.util.Random;

public class Zadanie5 {
    public static void main(String[] args) {
        int[] tablica1=new int[10];
        int[] tablica2=new int[10];
        int[] tablica3=new int[10];

        Random generator=new Random();

        for(int x=0;x<10;x++){
            tablica1[x]=generator.nextInt(20);
            tablica2[x]=generator.nextInt(20);
        }
        System.out.println();
        System.out.println("Zawartość 1 tablicy: ");

        for(int x=0;x<10;x++) {
            System.out.println(tablica1[x]);
        }

        System.out.println();
        System.out.println("Zawartość 2 tablicy: ");

        for(int x=0;x<10;x++) {
            System.out.println(tablica2[x]);
        }

        for(int x=0;x<10;x++){
            tablica3[x]=tablica1[x]+tablica2[x];
        }

        System.out.println("Zawartość trzeciej tablicy:");

        for(int x=0;x<10;x++){
            System.out.println(tablica3[x]);
        }
    }
}
