import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {

        int n;

        Scanner wejscie=new Scanner(System.in);
        System.out.println("Ile chcesz wprowadzić liczb ?");
        n=wejscie.nextInt();

        int[] tablica=new int[n];

        for(int x=0;x<n;x++){
            System.out.println("Wprowadź " + (x+1) + " liczbę: ");
            tablica[x]=wejscie.nextInt();
        }

        System.out.println();
        System.out.println("Wprowadzone liczby to: ");

        for(int x=0;x<n;x++){
            System.out.println(tablica[x]);
        }

    }
}
