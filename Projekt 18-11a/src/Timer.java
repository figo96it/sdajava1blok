public class Timer implements Runnable {
    public void run() {
        int time = 0;

        while(true) {
            try {
            Thread.sleep(1000);
        } catch(InterruptedException exc){
            System.out.println("Watek zostal przerwany");
            return;
        }
        time++;
        int minutes = time / 60;
        int sec = time % 60;
        System.out.println(minutes + ":" + sec);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Timer t=new Timer();
        Thread th=new Thread(t);
        th.start();

        Thread.sleep(500);
        Thread th2=new Thread();
        th2.start();

        Thread.sleep(10000);
        t.finish();

    }

}


