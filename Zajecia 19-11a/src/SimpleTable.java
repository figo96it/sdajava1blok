import javax.xml.crypto.Data;

public class SimpleTable<T> {

    private T[] data;

    public SimpleTable() {
        data = (T[])new Object[0];
    }

    public void add(T... values) {
        T[] newData = (T[])new Object[data.length + values.length];
        for (int i = 0; i < data.length; i++) {
            newData[i] = data[i];
        }
        for (int i = 0; i < values.length; i++) {
            newData[data.length + i] = values[i];
        }
        data=newData;
    }

    public String toString(){
        String result="";
        for(int i=0;i<data.length;i++){
            result=result+"["+data[i]+"]";
        }
        return result;
    }

    public static void main(String[] args) {
        SimpleTable<Integer> table=new SimpleTable<>();
        table.add(2,4,6,8,12);
        table.add();
        System.out.println(table);

        SimpleTable<String> stringTable=new SimpleTable<>();
        stringTable.add("jablko","gruszka","banan");
        System.out.println(stringTable);

        SimpleTable<Pair<Integer, String>> pairTable=new SimpleTable<>();
        pairTable.add(new Pair<>(1,"jablko"));
        pairTable.add(new Pair<>(2,"gruszka"));
        pairTable.add(new Pair<>(4,"banan"));
        pairTable.add(new Pair<>(6,"mandarynka"));
    }
}
