public class Point2 extends Figure {

    private double x,y;

    public Point2(){
        super(FigureType.POINT2);
        x=0.0;
        y=0.0;
    }

    public Point2(double x, double y){
        super(FigureType.POINT2);
        this.x=x;
        this.y=y;
    }

    public Point2(Point2 point){
        super(FigureType.POINT2);
        this.x=point.x;
        this.y=point.y;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public Point2[] getEdges(){
        Point2[] result=new Point2[1];
        result[0]= this;
        return result;
    }

    public double getArea() {
        return 0.0;
    }

    public double getCircuit() {
        return 0.0;
    }

    public void addEdge(Point2 p) {};
}
