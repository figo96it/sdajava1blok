public class Line extends Figure {

    private Point2 start, end;

    private void sort(){
        if(start.getX() > end.getX() || (start.getX() == end.getX()) && start.getY() > end.getY()){
            Point2 temp=start;
            start=end;
            end=temp;
        }
    }

    public Line(){
        super(FigureType.LINE);
        start=new Point2();
        end=new Point2();
    }

    public Line(Point2 start, Point2 end){
        super(FigureType.LINE);
        this.start=new Point2(start);
        this.end=new Point2(end);
        sort();
    }

    public Line(Line line){
        this(line.start, line.end);
    }

    public Point2 getStart() {
        return start;
    }

    public Point2 getEnd() {
        return end;
    }

    public void setStart(Point2 start) {
        this.start = start;
        sort();
    }

    public void setEnd(Point2 end) {
        this.end = end;
        sort();
    }

    public Point2[] getEdges(){
        Point2[] result=new Point2[2];
        result[0]=start;
        result[1]=end;
        return result;
    }

    public double getArea() {
        return 0.0;
    }

    public double getCircuit() {
        return 0.0;
    }

    public void addEdge(Point2 p) {};
}

