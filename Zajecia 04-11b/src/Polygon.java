public class Polygon extends Figure {

    private Point2[] edges;

    public Polygon(){
        super(FigureType.POLYGON);
        edges=null;
    }

    public Polygon(Polygon p){
        super(FigureType.POLYGON);
        edges=new Point2[p.edges.length];
        for(int i=0;i<p.edges.length;i++){
            edges[i]=p.edges[i];
        }
    }

    public void addEdge(Point2 p){
        if(edges==null){
            edges=new Point2[1];
            edges[0]=p;
            return;
        }

        Point2[] new_edges=new Point2[edges.length+1];

        for(int i=0; i<edges.length; i++){
            new_edges[i]=edges[i];
        }

        new_edges[edges.length]=p;
        edges=new_edges;
    }

    public void removeEdge(int index){
        if(index>edges.length) return;

        Point2[] new_edges=new Point2[edges.length-1];

        for(int i=0;i<index-1;i++){
            new_edges[i]=edges[i];
        }
        for(int i=index;i<edges.length;i++){
            new_edges[i-1]=edges[i];
        }

        edges=new_edges;
    }

    public Point2 getEdge(int index){
        if(index>edges.length) return null;
        return edges[index-1];
    }

    public void setEdge(int index,Point2 p){
        if(index>edges.length) return;
        edges[index-1]=p;
    }

    public Point2[] getEdges(){
        return edges;
    }

    public double getArea() {
        return 0.0;
    }

    public double getCircuit() {
        return 0.0;
    }
}
