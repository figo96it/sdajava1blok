public class Figure {

    public enum FigureType{NONE, POINT, LINE, CIRCLE, RECTANGLE, SQUARE, POLYGON};

    private FigureType type;
    public Figure(){
        type=FigureType.NONE;
    }

    public Figure(FigureType type){
        this.type=type;
    }

    public FigureType getType() {
        return type;
    }

    public void setType(FigureType type) {  //tylko obiekty klas pochodnych będą mogły zmodyfikować typ
        this.type = type;
    }

    


}