import java.util.Scanner;

public class Petle4 {
    public static void main(String[] args) {

        int dod=0, parz=0;
        Scanner cin=new Scanner(System.in);
        System.out.println("Wprowadź 10 liczb całkowitych.");
        for(int i=0;i<10;i++){
            System.out.println("Wprowadz "+(i+1)+" liczbę:");
            int x=cin.nextInt();
            if(x>0){
                dod=dod+1;
            }
            if((x%2)==0){
                parz=parz+1;
            }
        }
        System.out.println("Jest "+dod+" liczb dodatnich.");
        System.out.println("Natomiast parzystych liczb jest "+parz);
    }
}
