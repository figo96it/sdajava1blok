import java.util.Scanner;

import static java.lang.Math.abs;

public class if1 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Wprowadź liczbę całkowitą: ");
        int x=cin.nextInt();

        System.out.println();
        if((x%2)==0){
            System.out.println("Liczba " +x+ " jest parzysta.");
        }
        else{
            System.out.println("Liczba " +x+ " jest nieparzysta.");
        }

        System.out.println();
        if(x<0){
            x=abs(x);
            System.out.println("Wartość bezwzględna z danego argumentu wynosi " +x);
        }
        else{
            System.out.println("Wartość bezwzględna z danego argumentu wynosi " +x);
        }
    }
}
