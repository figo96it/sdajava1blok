import java.util.Scanner;

public class Petle8 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Program służy do wyznaczania silni wprowadzonej liczby n.");
        System.out.println("Wprowadź liczbę n: ");
        int x=cin.nextInt();

        int silnia=1;
        for(int i=1;i<=x;i++){
            silnia=silnia*i;
        }
        System.out.println("Silnia wynosi: "+silnia);
    }

}
