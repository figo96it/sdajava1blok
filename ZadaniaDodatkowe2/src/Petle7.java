import java.util.Scanner;

public class Petle7 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Program sprawdza czy podana liczba naturalna jest liczbą pierwszą.");
        System.out.println("Podaj liczbę naturalną: ");
        int x=cin.nextInt();
        int suma=0;

        for(int i=1;i<=x;i++){
            if((x%i)==0){
                suma=suma+1;
            }
        }

        if(suma<3){
            System.out.println("Podana liczba jest liczbą pierwszą.");
        }
        else{
            System.out.println("Podana liczba nie jest liczbą pierwszą.");
        }
    }
}
