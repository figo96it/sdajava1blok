import java.util.Scanner;

public class Petle5 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Program służy do zliczania sumy wszystkich liczb");
        System.out.println("całkowitych z przedziału [a,b].");
        System.out.println("Podaj początek przedziału: ");
        int a=cin.nextInt();
        System.out.println("Podaj koniec przedziału: ");
        int b=cin.nextInt();

        int suma=0;
        for(int i=a;i<=b;i++){
            suma=suma+i;
        }

        System.out.println("Suma liczb znajdujących się w przedziale");
        System.out.println("["+a+","+b+"] wynosi "+suma);
    }
}
