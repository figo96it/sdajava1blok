import java.util.Scanner;

public class Petla6 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Program sprawdza znajomość tabliczki mnożenia.");
        System.out.println("Podaj pierwszą liczbę: ");
        int x=cin.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int y=cin.nextInt();
        System.out.println();
        int iloczyn=x*y;
        int iloczyn2;
        do{
            System.out.println("Podaj ile wynosi iloczyn liczb: ");
            iloczyn2=cin.nextInt();
        }while(iloczyn!=iloczyn2);
        System.out.println("Brawo, odgadłeś! Iloczyn jest równy "+iloczyn2);
    }
}
