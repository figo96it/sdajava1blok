import java.util.Scanner;

public class switch1 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        String kod;

        System.out.println("Wprowadź kod języka (PL, EN, DE, FR).");
        kod=cin.nextLine();
        System.out.println();

        switch(kod){
            case "PL":
                System.out.println("Dzień Dobry. Witam serdecznie.");
                break;
            case "EN":
                System.out.println("Good morning. Welcome!");
                break;
            case "DE":
                System.out.println("Guten Morgen. Herzlich Willkommen!");
                break;
            case "FR":
                System.out.println("Bonne matin. Bienvenue!");
                break;
            default:
                System.out.println("No code was selected. Good morning. Welcome!.");
        }

    }
}
