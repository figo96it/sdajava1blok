import java.util.Scanner;

public class if4 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Ile punktów uzyskałeś z testu? (0-100)");
        double x=cin.nextDouble();

        if(x<50){
            System.out.println("Ocena: 1. Test nie zaliczony.");
        }
        else if((x>49)&(x<60)){
            System.out.println("Ocena: 2. Test zaliczony. Gratulacje.");
        }
        else if((x>59)&(x<70)){
            System.out.println("Ocena: 3. Test zaliczony. Gratulacje.");
        }
        else if((x>69)&(x<80)){
            System.out.println("Ocena: 4. Test zaliczony. Gratulacje.");
        }
        else if((x>79)&(x<90)){
            System.out.println("Ocena: 4,5. Test zaliczony. Gratulacje.");
        }
        else if((x>89)&(x<100)){
            System.out.println("Ocena: 5. Test zaliczony. Gratulacje.");
        }
        else if(x==100){
            System.out.println("Ocena: 6. Test zaliczony. Gratulacje.");
        }
    }
}
