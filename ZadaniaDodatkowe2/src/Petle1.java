public class Petle1 {
    public static void main(String[] args) {
        System.out.println("Drukowanie za pomoca petli for.");
        for(int i=0;i<100;i++){
            System.out.print(i+1);
            if(i<99){
                System.out.print(", ");
            }
        }
        System.out.println();
        System.out.println("Drukowanie za pomoca petli while.");
        int x=0;
        while(x<100){
            System.out.print(x+1);
            if(x<99){
                System.out.print(", ");
            }
            x++;
        }

        System.out.println();
        System.out.println("Drukowanie za pomoca petli do while.");

        int y=0;
        do{
            System.out.print(y+1);
            if(y<99){
                System.out.print(", ");
            }
            y++;
        }while(y<100);
    }
}
