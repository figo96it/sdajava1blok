import java.util.Scanner;

public class Petle9 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        System.out.println("Program służy do wyswietlania n kolejnych potęg naturalnych liczby 2.");
        System.out.println("Do której potęgi chcesz podniść liczbę 2?");
        int n=cin.nextInt();
        int x=2,potega=x;

        for(int i=1;i<=n;i++){
            if(i>=2){
                potega=potega*x;
            }
        }
        System.out.println();
        System.out.println("Liczba 2 podniesiona do potęgi "+n+" wynosi "+potega);

    }
}
