public class NegativeNumberException extends Exception {

    private int negativeNumber;

    public NegativeNumberException(int number){
        negativeNumber=number;
    }

    public int getNegativeNumber() {
        return negativeNumber;
    }
}
