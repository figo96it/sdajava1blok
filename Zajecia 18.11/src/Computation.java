public interface Computation {

     public double compute(double a, double b);

}
