public class Kot implements Zwierze {
    double masa;

    public String jedz(){
        return "Jem kocia karme...";
    }

    public Kot rozmnozsie(){
        Kot malyKot=new Kot();
        malyKot.masa=500;
        return malyKot;
    }

    public void rosnij(double masa){
        System.out.println("Rosne o "+masa);
        this.masa+=masa;
    }
}
