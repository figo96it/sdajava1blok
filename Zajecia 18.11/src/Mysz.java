public class Mysz implements Zwierze {
    double masa;

    public String jedz(){
        return "Jem zaiernka...";
    }

    public Mysz rozmnozsie(){
        Mysz malaMysz=new Mysz();
        malaMysz.masa=100;
        return malaMysz;
    }

    public void rosnij(double masa){
        System.out.println("Rosne o "+masa);
        this.masa+=masa;
    }

    public void piszcz(){
        System.out.print("PIIII PIII");
    }
}
