import java.util.InputMismatchException;
import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        Computation dzialanie;
        double a,b;
        int wybor;
        try {
            System.out.println("Jakie dzialanie Cie interesuje? <1-mnożenie, 2-potęgowanie>");
            wybor = cin.nextInt();

            if (wybor == 1) {
                dzialanie = new Multiplication();
            } else if (wybor == 2) {
                dzialanie = new Exponentiation();
            } else {
                dzialanie = null;
            }

            System.out.println("Czynnik A : ");
            a = cin.nextDouble();
            System.out.println("Czynnik B : ");
            b = cin.nextDouble();

            System.out.println("Wynik: " + dzialanie.compute(a, b));
        } catch(InputMismatchException e){
            System.out.println("Bład wprowadzonych danych. Sprobuj jeszcze raz.s");
        } catch(NullPointerException e){
            System.out.println("Dzialanie spoza dostepnych. Spróbuj jeszcze raz.");
        }
    }
}
