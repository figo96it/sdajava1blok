import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        Zwierze[] zwierzeta;

        System.out.println("Ile chcesz zwierzat ?");
        int len=cin.nextInt();

        zwierzeta=new Zwierze[len];
        for(int i=0;i<len;i++){
            System.out.println("Kot/Mysz/Pies? (1/2/3)");
            int choice=cin.nextInt();

            if(choice==1){
                zwierzeta[i]=new Kot();
            }
            else if(choice==1){
                zwierzeta[i]=new Kot();
            }
            else if(choice==2){
                zwierzeta[i]=new Mysz();
            }
            else if(choice==3){
                zwierzeta[i]=new Pies();
            }
            else {
                zwierzeta[i]=null;
            }
        }

        System.out.println();
        System.out.println("Tak jedza twoje zwierzatka: ");
        for(int i=0;i<len;i++){
            System.out.println("Zwierzatko "+i+": "+zwierzeta[i].jedz());
        }

        System.out.println();
        System.out.println("A tak piszczą twoje zwierzatka: ");
        for(int i=0;i<len;i++){
            System.out.print("Zwierzatko "+i+": ");
            zwierzeta[i].piszcz();
            System.out.println();
        }
    }
}
