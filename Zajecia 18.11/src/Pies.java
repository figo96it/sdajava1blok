public class Pies implements Zwierze {
    double masa;

    public String jedz(){
        return "Jem mieso...";
    }

    public Pies rozmnozsie(){
        Pies malyPies=new Pies();
        malyPies.masa=2000;
        return malyPies;
    }

    public void rosnij(double masa){
        System.out.println("Rosne o "+masa);
        this.masa+=masa;
    }

    public void piszcz(){
        System.out.print("HAUUU HAUU HAUU, grrrr");
    }
}
