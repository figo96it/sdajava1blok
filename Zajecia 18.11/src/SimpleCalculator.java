import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleCalculator {

    public static double dodawanie(double a,double b){
        return (a+b);
    }

    public static double odejmowanie(double a,double b){
        return (a-b);
    }

    public static double mnożenie(double a,double b){
        return (a*b);
    }

    public static double dzielenie(double a,double b){
        return (a/b);
    }

    public static double potega(double a, int b){
        double suma=0.0;

        for (int i = 1; i <=b ; i++) {
            suma = suma * a;
        }
        return suma;
    }

    public static double pierwiastek(double a) {
        double b = Math.sqrt(a);
        return b;
    }

    public static void main(String[] args) {

        Scanner cin=new Scanner(System.in);
        double a,b;

        while(true){
            try{
                System.out.println("Wprowadz liczbe a: ");
                a=cin.nextDouble();
                System.out.println("Wprowadz liczbe b: ");
                b=cin.nextDouble();
                System.out.println("Dodawanie: "+a+" + "+b+" = "+(a+b));
                System.out.println("Odejmowanie: "+a+" - "+b+" = "+(a-b));
                System.out.println("Mnożenie: "+a+" * "+b+" = "+(a*b));
                System.out.println("Dzielenie: "+a+" / "+b+" = "+(a/b));
            } catch(ArithmeticException e){
                System.out.println("Błąd obliczeń!");
            } catch(InputMismatchException e){
                System.out.println("Blad formatu wproawdzonych danych!");
            } catch(Exception e){
                System.out.println("Inny błąd działania!");
                 cin.nextLine();
            }
        }
    }
}
