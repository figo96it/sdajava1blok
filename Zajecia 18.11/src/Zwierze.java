import java.util.Scanner;

public interface Zwierze {
    public String jedz();
    public Zwierze rozmnozsie();
    public void rosnij(double masa);

    public default void piszcz () {
        System.out.print("Ja nie piszcze !!");
    }

}


