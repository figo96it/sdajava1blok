public class OutOfBoxException extends Exception {

    private int outNumber;

    public OutOfBoxException(int number){
        outNumber=number;
    }

    public int getOutNumber() {
        return outNumber;
    }
}
