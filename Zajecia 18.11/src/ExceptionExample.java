import java.util.Scanner;

public class ExceptionExample {

    public static void main(String[] args) {

        Scanner cin=new Scanner(System.in);
        int dzielnik, i=0;
        int[] dane={3,5,0,8,12};

        while (true) {
            try {
                System.out.println("Wprowadz dzielnik: ");
                dzielnik = cin.nextInt();
                System.out.println("Dzielenie " + dane[i] + "/" + dzielnik + "=" + (dane[i] / dzielnik));
            } catch (ArithmeticException e) {
                System.out.println("Błąd obliczeń!!");
            } catch(IndexOutOfBoundsException e){
                System.out.println("Błąd zakresu!!");
            } catch(Exception e){
                System.out.println("Inny błąd działania!");
                cin.nextLine();
            }

            i++;
        }
    }
}
