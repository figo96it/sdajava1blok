import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Birthday3 {

    public static void main(String[] args) {

        Scanner cin=new Scanner(System.in);
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        System.out.println("Wprowadz date w formacie DD/MM/YYYY: ");
        String toParse=cin.nextLine() + " 00:00";

        LocalDateTime birthday=LocalDateTime.parse(toParse, formatter);
        LocalDateTime actualDate=LocalDateTime.now();

        long diffDays = ChronoUnit.DAYS.between(birthday,actualDate);
        long diffMonths = ChronoUnit.MONTHS.between(birthday,actualDate);
        long diffYears = ChronoUnit.YEARS.between(birthday,actualDate);
        long diffHours = ChronoUnit.HOURS.between(birthday,actualDate);
        long diffMinutes = ChronoUnit.MINUTES.between(birthday,actualDate);
        long diffSeconds = ChronoUnit.SECONDS.between(birthday,actualDate);

        System.out.println("Żyjesz już: ");
        System.out.println(" - "+diffYears+" lat.");
        System.out.println(" - "+diffMonths+" miesięcy.");
        System.out.println(" - "+diffDays+" dni.");
        System.out.println(" - "+diffHours+" godzin.");
        System.out.println(" - "+diffMinutes+" minut.");
        System.out.println(" - "+diffSeconds+" sekund.");

    }
}