import java.util.Random;
import java.util.Scanner;
import java.util.Locale;


public class Tablice {
    public static void pobierz(double[] T){
        Scanner s=new Scanner(System.in);
        s.useLocale(Locale.US);

        for(int i=0;i<T.length;i++){
            System.out.println("Podaj " +(i+1)+ " liczbe.");
            T[i]=s.nextDouble();
        }

    }

    public static void losuj(double[] T){
        Random r=new Random();

        for(int i=0;i<T.length;i++){
            T[i]=r.nextDouble() * 1000;
        }
    }

    public static void wypisz(double[] T){
        System.out.print("T=[ ");
        for(int i=0;i<T.length;i++){
            System.out.print(T[i]);
            if(i<T.length-1){
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

    public static double suma(double[] T){
        double suma=0.0;

        for(int i=0;i<T.length;i++){
            suma=suma+T[i];
        }
        return suma;
    }

    public static double srednia(double[] T){

        return suma(T)/T.length;
    }

    public static double minimum(double[] T){
        double min=T[0];
        for(int i=0;i<T.length;i++){
            if (T[i] < min) {
                min=T[i];
            }
        }
        return min;

    }

    public static double maximum(double[] T){
        double max=T[0];
        for(int i=0;i<T.length;i++){
            if (T[i] > max) {
                max=T[i];
            }
        }
        return max;

    }

}
