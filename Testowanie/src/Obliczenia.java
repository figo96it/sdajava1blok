public class Obliczenia {
    public static double suma(double a, double b){
        return a+b;
    }

    public static double srednia(double a, double b){
        return suma(a,b)/2.0;
    }

    public static double minimum(double a,double b){
        if (a <= b) {
            return a;
        }
        else{
            return b;
        }
    }

    public static double maximum(double a,double b){
        if(a>=b){
            return a;
        }
        else{
            return b;
        }
    }
}
