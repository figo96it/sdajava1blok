import java.util.Locale;
import java.util.Scanner;

public class Testowanie {
    public static void main(String[] args) {

        Scanner cin=new Scanner(System.in);
        cin.useLocale(Locale.US);

        double x,y;

        System.out.println("Wprowadz A: ");
        x=cin.nextDouble();
        System.out.println("Wprowadz B: ");
        y=cin.nextDouble();

        System.out.println("SUMA: " + Obliczenia.suma(x, y));
        System.out.println("SREDNIA: " + Obliczenia.srednia(x, y));
        System.out.println("MINIMUM: " + Obliczenia.minimum(x, y));
        System.out.println("MAXIMUM: " + Obliczenia.maximum(x, y));

        System.out.println();
        System.out.println("----------");
        System.out.println();

        System.out.println("Ile elementow chcesz wprowadzic do tablicy?");
        int n=cin.nextInt();
        double[] tablica=new double[n];

        Tablice.losuj(tablica);
        Tablice.wypisz(tablica);

        System.out.println("SUMA: " + Tablice.suma(tablica));
        System.out.println("SREDNIA: " + Tablice.srednia(tablica));
        System.out.println("MINIMUM: " + Tablice.minimum(tablica));
        System.out.println("MAXIMUM: " + Tablice.maximum(tablica));


        System.out.println();
        System.out.println("----------");
        System.out.println();

        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int a=cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int b=cin.nextInt();
        double[][] macierz=new double[a][b];

        TabliceWielowymiarowe.losuj(macierz);
        TabliceWielowymiarowe.wypisz(macierz);
        System.out.println("SUMA: " + TabliceWielowymiarowe.suma(macierz));

        if(a==b){
            System.out.println("SUMA PRZEKATNEJ: " + TabliceWielowymiarowe.przekatna(macierz));
        }
    }
}
