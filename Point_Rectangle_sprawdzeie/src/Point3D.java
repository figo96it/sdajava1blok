public class Point3D {

    private int x;
    private int y;
    private int z;

    public Point3D(){
        x=0;
        y=0;
        z=0;
    }

    public Point3D(int x, int y, int z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public void set(int x, int y, int z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public String toString(){
        return "{"+x+", "+y+", "+z+"}";
    }

    public boolean equals(Point3D p){
        boolean result;
        if(x==p.x && y==p.y && z==p.z){
            result=true;
        }
        else{
            result=false;
        }
        return result;
    }

    public void movePoint(int mx, int my, int mz){
        x=x+mx;
        y=y+my;
        z=z+mz;
    }

    public void clear(){
        x=0;
        y=0;
        z=0;
    }

    public double distance(Point3D p){
        double result=Math.pow((p.x-x),2)+Math.pow((p.y-y),2)+Math.pow((p.z-z),2);
        result=Math.sqrt(result);
        return result;
    }


}
