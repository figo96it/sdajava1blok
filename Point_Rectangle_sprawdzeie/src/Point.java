import static java.lang.Math.pow;

public class Point {
    private int x;
    private int y;

    public Point(){
        x=0;
        y=0;
    }

    public Point(int x, int y){
        this.x=x;
        this.y=y;
    }

    public Point(Point p){
        x=p.x;
        y=p.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString(){
        return "{"+x+", "+y+"}";
    }

    public boolean equals(Point p){
        boolean result;
        if((x==p.x) && (y==p.y)){
            result=true;
        }
        else{
            result=false;
        }

        return result;
    }

    public void movePoint(int mx, int my){
        x=x+mx;
        y=y+my;
    }

    public double distance(Point p){
        double result=0.0;
        result=pow((p.x-x),2);
        result=result+ pow((p.y-y),2);
        return Math.sqrt(result);
    }

    public void clear(){
        x=0;
        y=0;
    }

    public int quarter(){
        int i=0;
        if(x>=0 && y>=0){
            i++;
        }
        else if(x<0 && y>=0){
            i++;
        }
        else if(x<0 && y<0){
            i++;
        }
        else if(x>=0 && y<0){
            i++;
        }
        return i;
    }
}