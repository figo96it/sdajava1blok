import java.util.Random;

public class Circle {

    private Point p;
    private double r;

    public Circle(){
        p=new Point();
        r=0.0;
    }

    public Circle(int x,int y,double r){
        p=new Point(x,y);
        this.r=r;
    }

    public Circle(Point p,double r){
        this.p=new Point(p);
        this.r=r;
    }

    public Circle(Circle t){
        p=new Point(t.p);
        r=t.r;
    }

    public double getR() {
        return r;
    }

    public Point getP() {
        return p;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void setP(int x, int y) {
        p=new Point(x,y);
    }

    public String toString(){
        return ""+p+"-->"+r;
    }

    public boolean equals(Circle p){
        return r==p.r;
    }

    public double getDiameter(){
        return 2*r;
    }

    public double getArea(){
        return Math.PI*r*r;
    }

    public double getCircuit(){
        return 2.0*Math.PI*r;
    }

    public boolean isInside(Point p2){
        return p.distance(p2)<=r;
    }

    public boolean isInside(Point[] points){
        for(Point a: points){
            if(!isInside(a)) return false;
        }
        return true;

    //    for(int i=0;i<points.length;i++){
    //        if(!isInside(points[i])) return false;
    //    }

    }

    public boolean isOverLapped(Circle c){
        return p.distance(c.p)<=r+c.r;
    }
