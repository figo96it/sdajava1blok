public class Points{
    private Point[] data;
    private int counter;

    public Points(){
        data= new Point[3];
        counter=0;
    }

    public Points(int size){
        data= new Point[size];
        counter=0;
    }

    public Points(Point[] p){
        data=new Point[p.length];
        for(int i=0;i<p.length;i++){
            data[i]=new Point(p[i]);
        }
        counter=data.length;
    }

    public Points(Points p){
        data= new Point[p.data.length];
        for(int i=0;i< p.data.length; i++){
            data[i]=new Point(p.data[i]);
        }
        counter=data.length;
    }

    public Point getPoint(int i){
        return new Point(data[i]);
    }

    public void addPoint(Point p){
        if(counter==data.length) return;
        for(int i=0;i<data.length;i++){
            if(data[i]==null){
                counter++;
                data[i]=new Point(p);
                return;
            }
        }
    }

    public void deletePoint(Point p){
        if(p==null) return;
        for(int i=0;i<data.length;i++){
            if(data[i]!=null && data[i].equals(p)){
                data[i]==null;
                counter--;
            }
        }
    }

    public void setPoint(Point p, int i){
        if(i>=data.length) return;
        if(data[i]==null) counter++;
        data[i]=new Point(p);
    }

        String result=""+counter+": ";
        for(int i=0;i<data.length;i++){
            result=result+" "+data[i];
        }
        return result;
    }

    public double usedPercentage(){
        return (double)counter/(double)data.length*100;
    }

    public void changeSize(int newSize){

    }
}