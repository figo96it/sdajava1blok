import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Actions extends JFrame implements ActionListener {

    JButton bAction1, bAction2;
    JTextField textArea;

    public Actions() {
        super( "Hello World" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 600);
        setLocation(50,50);
        setLayout(new FlowLayout());

        bAction1 = new JButton("akcja 1");
        bAction1.addActionListener(this);
        bAction2 = new JButton("akcja 2");
        bAction2.addActionListener(this);

        textArea = new JTextField("Teskt domyślny");

        add(bAction1);
        add(bAction2);
        add(textArea);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(bAction1)) {
            System.out.println("akcja 1");
        }
        if (e.getSource().equals(bAction2)) {
            System.out.println("akcja 2");
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Actions();
            }
        });
    }
}