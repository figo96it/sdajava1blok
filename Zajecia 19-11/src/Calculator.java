import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements ActionListener {

    JButton bAction1, bAction2, bAction3, bAction4;
    JTextField textArea1, textArea2;
    JLabel label1,label2,label3;
    JPanel panel1,panel2, panel3;

    public Calculator() {
        super( "Hello World" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 140);
        setLocation(50,50);
        setLayout(new BorderLayout());

        panel1=new JPanel();
        bAction1 = new JButton("+");
        bAction1.addActionListener(this);
        bAction2 = new JButton("-");
        bAction2.addActionListener(this);
        bAction2 = new JButton("*");
        bAction2.addActionListener(this);
        bAction2 = new JButton("/");
        bAction2.addActionListener(this);
        panel1.add(bAction1);
        panel1.add(bAction2);
        panel1.add(bAction3);
        panel1.add(bAction4);

        panel2=new JPanel();
        label1 = new JLabel("Czynnik 1: ");
        label2 = new JLabel("Czynnik 2: ");
        textArea1 = new JTextField();
        textArea1.setPreferredSize(new Dimension(50,20));
        textArea2 = new JTextField();
        textArea2.setPreferredSize(new Dimension(50,20));
        panel2.add(label1);
        panel2.add(textArea1);
        panel2.add(label2);
        panel2.add(textArea2);

        panel3=new JPanel();
        label3=new JLabel("Wynik: ");
        panel3.add(label3);
//        label3.setPreferredSize(new Dimension(100,20));

        add(panel2, BorderLayout.NORTH);
        add(panel1, BorderLayout.CENTER);
        add(panel3, BorderLayout.SOUTH);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(bAction1)) {
            System.out.println("akcja 1");
        }
        if (e.getSource().equals(bAction2)) {
            System.out.println("akcja 2");
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Actions();
            }
        });
    }
}