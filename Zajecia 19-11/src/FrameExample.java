import javax.swing.*;

public class FrameExample {

    public static void main(String[] args) {
        JFrame window=new JFrame("Hello World");
        window.setSize(400,600);
        JButton b=new JButton("Click me,please!");
        b.setSize(200,200);
        JButton b2=new JButton("Click me too");
        b2.setSize(400,400);
        window.add(b);
        window.add(b2);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
    }
}
