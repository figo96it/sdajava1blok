import javax.swing.*;
import java.awt.*;

public class HelloWorld extends JFrame{
    public HelloWorld() {
        super("Hello World!");
        setSize(400, 600);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run(){
                new HelloWorld();
            }
        });
    }
}