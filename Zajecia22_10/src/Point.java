public class Point {

    private int x, y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        x = p.x;
        y = p.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return "{" + x + ", " + y + "}";
    }

    public boolean equals(Point p) {
        return (x == p.x && y == p.y);
    }

    public void move(int a, int b) {
        x = x + a;
        y = y + b;
    }

    public double distance(Point p) {
        double result = (x - p.x)*(x - p.x) + (y - p.y)*(y - p.y);
        result = Math.sqrt(result);
        return result;
    }

    public int quarter() {
        if (x >= 0 && y >= 0) {
            return 1;
        } else if (x < 0 && y >= 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else {
            return 4;
        }
    }
}