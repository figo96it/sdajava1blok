import java.util.Scanner;

public class Test {

    private static String menu = "Cześć. Co chcesz zrobić?\n" +
            "1. Wprowadz dane prostokąta 1.\n" +
            "2. Zmień lewy-górny punkt prostokata 1.\n" +
            "3. Zmień prawy-górny punkt prostokata 1.\n" +
            "4. Zmień lewy-dolny punkt prostokata 1.\n" +
            "5. Zmień prawy-dolny punkt prostokata 1.\n" +
            "6. Wprowadz dane prostokata 2.\n" +
            "7. Zmień lewy-górny punkt prostokata 2.\n" +
            "8. Zmień prawy-górny punkt prostokata 2.\n" +
            "9. Zmień lewy-dolny punkt prostokata 2.\n" +
            "10. Zmień prawy-dolny punkt prostokata 2. \n" +
            "11. Wprowadź dane punktu 1\n" +
            "12. Wprowadź dane punktu 2\n" +
            "13. Przesuń punkt 1 o wektor\n" +
            "14. Przesuń punkt 2 o wektor\n" +
            "15. Oblicz odległość między punktami\n" +
            "16. W której ćwiartce są punkty ?\n" +
            "17. Czy punkt znajduje się w prostokącie?\n" +
            "18. Czy prostokąty nachodzą na siebie?\n" +
            "19. Czy dany prostokąt jest kwadratem?\n" +
            "0. KONIEC.\n";

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Rectangle r1, r2;
        Point p1, p2;

        r1 = new Rectangle();
        r2 = new Rectangle();
        p1 = new Point();
        p2 = new Point();
        int choice;
        int a, b;

        do {
            System.out.println(menu);
            choice = cin.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Lewy-górny X= ");
                    r1.setX1(cin.nextInt());
                    System.out.println("Lewy-górny Y= ");
                    r1.setY1(cin.nextInt());
                    System.out.println("Prawy-dolny X= ");
                    r1.setX2(cin.nextInt());
                    System.out.println("Prawy-dolny Y= ");
                    r1.setY2(cin.nextInt());
                    break;
                case 2:
                    System.out.println("Lewy-górny X = ");
                    r1.setX1(cin.nextInt());
                    System.out.println("Lewy-górny Y = ");
                    r1.setY1(cin.nextInt());
                    System.out.println(r1);
                    break;
                case 3:
                    System.out.println("Prawy-górny X = ");
                    r1.setX3(cin.nextInt());
                    System.out.println("Prawy-górny Y = ");
                    r1.setY3(cin.nextInt());
                    System.out.println(r1);
                    break;
                case 4:
                    System.out.println("Lewy-dolny X = ");
                    r1.setX4(cin.nextInt());
                    System.out.println("Lewy-dolny Y = ");
                    r1.setY4(cin.nextInt());
                    System.out.println(r1);
                    break;
                case 5:
                    System.out.println("Prawy-dolny X = ");
                    r1.setX2(cin.nextInt());
                    System.out.println("Prawy-dolny Y = ");
                    r1.setY2(cin.nextInt());
                    System.out.println(r1);
                    break;
                case 6:
                    System.out.println("Lewy-górny X= ");
                    r2.setX1(cin.nextInt());
                    System.out.println("Lewy-górny Y= ");
                    r2.setY1(cin.nextInt());
                    System.out.println("Prawy-dolny X= ");
                    r2.setX2(cin.nextInt());
                    System.out.println("Prawy-dolny Y= ");
                    r2.setY2(cin.nextInt());
                    break;
                case 7:
                    System.out.println("Lewy-górny X = ");
                    r2.setX1(cin.nextInt());
                    System.out.println("Lewy-górny Y = ");
                    r2.setY1(cin.nextInt());
                    System.out.println(r2);
                    break;
                case 8:
                    System.out.println("Prawy-górny X = ");
                    r2.setX3(cin.nextInt());
                    System.out.println("Prawy-górny Y = ");
                    r2.setY3(cin.nextInt());
                    System.out.println(r2);
                    break;
                case 9:
                    System.out.println("Lewy-dolny X = ");
                    r2.setX4(cin.nextInt());
                    System.out.println("Lewy-dolny Y = ");
                    r2.setY4(cin.nextInt());
                    System.out.println(r2);
                    break;
                case 10:
                    System.out.println("Prawy-dolny X = ");
                    r2.setX2(cin.nextInt());
                    System.out.println("Prawy-dolny Y = ");
                    r2.setY2(cin.nextInt());
                    System.out.println(r2);
                    break;
                case 11:
                    System.out.print("Wprowadź X: ");
                    p1.setX(cin.nextInt());
                    System.out.print("Wprowadź Y: ");
                    p1.setY(cin.nextInt());
                    break;
                case 12:
                    System.out.print("Wprowadź X: ");
                    p2.setX(cin.nextInt());
                    System.out.print("Wprowadź Y: ");
                    p2.setY(cin.nextInt());
                    break;
                case 13:
                    System.out.print("Wprowadź X wektora: ");
                    a = cin.nextInt();
                    System.out.print("Wprowadź Y wektora: ");
                    b = cin.nextInt();
                    p1.move(a, b);
                    break;
                case 14:
                    System.out.print("Wprowadź X wektora: ");
                    a = cin.nextInt();
                    System.out.print("Wprowadź Y wektora: ");
                    b = cin.nextInt();
                    p2.move(a, b);
                    break;
                case 15:
                    System.out.println("Odległość: " + p1.distance(p2));
                    break;
                case 16:
                    System.out.println("Punkt 1: " + p1 + ", ćwiartka: " + p1.quarter());
                    System.out.println("Punkt 2: " + p2 + ", ćwiartka: " + p2.quarter());
                    break;
                case 17:

                case 19:
                    System.out.println("Czy prostokąt 1 jest kwadratem? " + r1.isSquare());
                    System.out.println("Czy prostokąt 2 jest kwadratem? " + r2.isSquare());
                case 0:
                    System.out.println("KONIEC !!!");
                    break;
                default:
                    System.out.println("Zle wprowadziles dane.");
                    break;
            }
        } while (choice > 0);
    }
}
