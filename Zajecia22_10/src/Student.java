public class Student {
    private String firstName;
    private String lastName;
    private int age;
    private int[] score;

    public static int counter=0;

    public Student(){   //konstruktor domyślny czyli bezparametrowy
        firstName="";
        lastName="";
        age=0;

        counter++;
    }

    public Student(String first, String last){   //konstruktor z parametrami
        firstName=first;
        lastName=last;
        age=0;

        counter++;
    }

    public Student(String first, String last, int a){     //konstruktor z parametrami
        firstName=first;
        lastName=last;
        age=a;

        counter++;
    }

    public Student(Student s){  //konstruktor 'kopiujący' -tworzy nowy obiekt jako kopię obiektu z parametru
        firstName=s.firstName;
        lastName=s.lastName;
        age=s.age;

        counter++;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String toString(){
        if(firstName.equals("")){
            return "<dane studenta są niekompletne> (niepoprawne imię)";
        }
        if(lastName.equals("")){
            return "<dane studenta są niekompletne> (niepoprawne nazwisko)";
        }

        String result=""+firstName+" "+lastName;
        if(age>0){
            result=result+", lat: "+age;
        }
        else{
            result=result+" <dane studenta są niekompletne> (niepoprawny wiek)";
        }

        return result;
    }

    public static void main(String[] args) {
        Student s1,s2,s3,s4;
        s1=new Student();
        s2=new Student("Anna","A.");
        s3=new Student("Beata","B.",25);

        s1.setFirstName("Lukasz");
        s1.setLastName("W.");
        s1.setAge(20);

        s4=new Student(s1);
        Student s5=new Student();

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        System.out.println(s5);
    }

}
