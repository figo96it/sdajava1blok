import javax.xml.bind.SchemaOutputResolver;

public class Obliczenia2 {

    public static final int MIN=-10;
    public static final int MAX=10;

    private static double ogranicz(double a){
        while(a>MAX){
            a=a-(MAX-MIN+1);
        }

        while(a<MIN){
            a=a+(MAX-MIN+1);
        }

        return a;
    }

    public static double suma(double a, double b){
        return ogranicz(a+b);
    }

    public static double srednia(double a, double b){
        return ogranicz(suma(a,b)/2.0);
    }

    public static double minimum(double a, double b){
        if(a<=b){
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }

    public static double maximum(double a, double b){
        if(a>=b){
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }

//    TESTOWANIE

    public static void main(String[] args) {
        System.out.println(suma(12,30));
        System.out.println(suma(-8,-5));
        System.out.println(suma(8,0));
        System.out.println(suma(-1,27));

    }
}
