import java.util.Scanner;

public class TShirt {
    public enum TShirtSizes{S, M, L, XL, XXL};
    public enum TShirtColours{White,Black,Blue,Red,Green};

    public static String available7Sizes(){
        String result="";
        for(int i=0;i<TShirtSizes.values().length;i++){
            result=result+TShirtSizes.values()[i];
            if(i<TShirtSizes.values().length-1){
                result=result+", ";
            }
        }
        result=result+">";
        return result;
    }

    public static String availableColour(){
        String result="";
        for(int i=0;i<TShirtColours.values().length;i++){
            result=result+TShirtColours.values()[i];
            if(i<TShirtColours.values().length-1){
                result=result+", ";
            }
        }
        result=result+">";
        return result;
    }

    private TShirtSizes size;
    private TShirtColours colour;
    private String producer;
    private String model;

    public TShirt(){
        size=TShirtSizes.M;
        colour=TShirtColours.Black;
        producer="Made in China";
        model="Universal";
    }

    public TShirt(TShirtSizes size,TShirtColours colour, String producer, String model){
        this.size=size;
        this.colour=colour;
        this.producer=producer;
        this.model=model;
    }

    public String getModel() {
        return model;
    }

    public String getProducer() {
        return producer;
    }

    public TShirtColours getColour() {
        return colour;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public void setColour(TShirtColours colour) {
        this.colour = colour;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public String toString(){
        return ""+size+" "+colour+" "+producer+": "+model;
    }

    public boolean equals(TShirt t2){
        if(size!=t2.size) return false;
        if(colour!=t2.colour) return false;
        if(!producer.equals(t2.producer)) return false;
        if(!model.equals(t2.model)) return false;
        return true;
    }

    //TESTOWANIE
    public static void main(String[] args) {
        TShirt t1=new TShirt();
        TShirt t2=new TShirt(TShirtSizes.XXL,TShirtColours.Green,"Made in USA","Big" );

        System.out.println(t1);
        System.out.println(t2);

        Scanner cin=new Scanner(System.in);
        String size,colour,producer,model;

        System.out.println("Twoja wymarzona koszulka!");
        System.out.println("Podaj rozmiar: ");
        size=cin.nextLine();
        System.out.println("Podaj kolor: ");
        colour=cin.nextLine();
        System.out.println("Podaj producenta: ");
        producer=cin.nextLine();
        System.out.println("Podaj model: ");
        model=cin.nextLine();

        size=size.toUpperCase();
        colour=colour.substring(0,1).toUpperCase()+colour.substring(1).toLowerCase();

        TShirt t3=new TShirt(TShirtSizes.valueOf(size),TShirtColours.valueOf(colour),producer,model);
        System.out.println(TShirtSizes.valueOf(size.toUpperCase()));
        System.out.println(TShirtColours.valueOf(colour));

        System.out.println(t3);
    }
}
