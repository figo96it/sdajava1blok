public class SimpleTable {

    private int[] numbers;

    public SimpleTable(int... numbers){
        this.numbers=new int[numbers.length];
        for(int i=0;i<numbers.length;i++){
            this.numbers[i]=numbers[i];
        }
    }

    @Override
    public String toString() {
        String result="";
        for(int i=0;i<numbers.length;i++){
            result=result+numbers[i];
            if(i<numbers.length-1){
                result=result+", ";
            }
        }
        return result;
    }

    public void addElement(int e){
        int[] newNumbers=new int[numbers.length+1];
        for(int i=0;i<numbers.length;i++){
            newNumbers[i]=numbers[i];
        }
        newNumbers[numbers.length]=e;
        numbers=newNumbers;
    }

    public void addElements(int... elems){
        if(elems.length==0) return;
        int[] newNumbers=new int[numbers.length+elems.length];
        for(int i=0;i<numbers.length;i++){
            newNumbers[i]=numbers[i];
        }
        for(int i=0;i<elems.length;i++){
            newNumbers[numbers.length+i]=elems[i];
        }
        numbers=newNumbers;
    }

    public void removeElement(int e){
        int counter=0;
        for(int i=0;i<numbers.length;i++){
            if(numbers[i]==e){
                counter++;
            }
        }

        int[] newNumbers=new int[numbers.length-counter];

        int j=0;
        for(int i=0;i<numbers.length;i++){
            if(numbers[i]!=e){
                newNumbers[j]=numbers[i];
                j++;
            }
        }
        numbers=newNumbers;
    }

    public void removeElements(int... elems){
        if(elems.length==0) return;

        if(elems.length==1){
            removeElement(elems[0]);
            return;
        }

        for(int i=0;i<numbers.length;i++){
            for(int j=1;j<elems.length;j++){
                if(numbers[i]==elems[j]){
                    numbers[i]=elems[0];
                }
            }
        }
        removeElement(elems[0]);
    }

    public static void main(String[] args) {
        SimpleTable a=new SimpleTable();
        SimpleTable b=new SimpleTable(2,4,8);

        System.out.println(a);
        System.out.println(b);

        a.addElements(3,5,6,10,12,5);
        System.out.println(a);
        a.addElement(17);
        a.addElements();
        a.addElements(9,14);
        System.out.println(a);
        a.removeElements(5,12,15,17);
        System.out.println(a);
    }
}
