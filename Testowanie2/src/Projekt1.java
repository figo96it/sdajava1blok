import java.util.Random;
import java.util.Scanner;

public class Projekt1 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        /*System.out.println("Wprowadz dane do 1 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int a=cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int b=cin.nextInt();
        double[][] macierz=new double[a][b];
        System.out.println();

        losuj(macierz);
        wypisz(macierz);

        System.out.println("Wprowadz dane do 2 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int c=cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int d=cin.nextInt();*/
        double[][] macierz=new double[2][3];
        double[][] macierz2=new double[3][2];
        System.out.println();

        losuj(macierz,macierz2);
        wypisz(macierz);
        System.out.println();
        wypisz(macierz2);

    }

    public static void losuj(double[][] T, double[][] F){
        Random r=new Random();

        for(int i=0;i<T.length;i++){
            for(int j=0;j<T.length;j++) {
                T[i][j] = r.nextDouble();
            }
        }

        for(int i=0;i<F.length;i++){
            for(int j=0;j<F.length;j++) {
                F[i][j] = r.nextDouble();
            }
        }
    }

    public static void wypisz(double[][] T){
        for(int i=0;i<T.length;i++){
            System.out.print("[");
            for(int j=0;j<T.length;j++){
                System.out.print(T[i][j]);
                if(j<T.length-1){
                    System.out.print(", ");
                }
            }
            System.out.println("]");
        }
    }
}
