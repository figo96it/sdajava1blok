public class Complex {

    private double real, image;

    public Complex(){
        real=0.0;
        image=0.0;
    }

    public Complex(double real, double image){
        this.real=real;
        this.image=image;
    }

    public Complex(Complex complex){
        this.real=complex.real;
        this.image=complex.image;
    }

    public double getReal() {
        return real;
    }

    public double getImage() {
        return image;
    }

    public void setImage(double image) {
        this.image = image;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public String toString(){
        return "Z= "+getReal()+" + "+getImage()+"i";
    }

    public boolean equals(Complex c){
        return ((real==c.real) && (image==c.image));
    }

    public String plus(Complex c){
        return "Z= "+(c.getReal()+this.getReal())+" + "+(c.getImage()+this.getImage())+"i.";
    }

    public String minus(Complex c){
        return "Z= "+(c.getReal()-this.getReal())+" + "+(c.getImage()-this.getImage())+"i.";
    }
}
