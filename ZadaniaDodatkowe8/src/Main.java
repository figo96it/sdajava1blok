public class Main {

    public static void main(String[] args) {
        // TEST COMPLEX
        Complex z1=new Complex();
        Complex z2=new Complex(5,4);

        z1.setImage(4);
        z1.setReal(5);

        System.out.println(z1);
        System.out.println(z2);

        System.out.println(z1.equals(z2));
        System.out.println(z2.equals(z1));

        System.out.println(z1.plus(z2));

        System.out.println(z1);
        System.out.println(z2);
        System.out.println(z1.minus(z2));
    }
}
