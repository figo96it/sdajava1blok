

public class Function {
    private int a,b,c;

    public Function(){
        a=0;
        b=0;
        c=0;
    }

    public Function(int a, int b, int c){
        this.a=a;
        this.b=b;
        this.c=c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String toString(){
        return "f(x) = " + getA() + "*x^2 + " + getB() + "*x " + " + " + getC();
    }

    public double getResult(int x){
        return ((getA()*x*x)+(getB()*x)+getC());
    }

    public double getDelta(){
        return Math.pow(getB(),2)-(4*getA()*getC());
    }

    public Point getPeak() {
        double delta = getDelta();
        if (delta >= 0) {
            return new Point((-getB() / (2 * getA())),(-delta) / (4 * getA()));
        } else {
            return null;
        }
    }



    public static void main(String[] args) {
        Function f=new Function();

        double result;
        f.setA(2);
        f.setB(-2);
        f.setC(10);

        result=f.getResult(5);
        System.out.println(result);
        System.out.println(f);
        System.out.println(f.getDelta());
        System.out.println(f.getPeak());
    }


}
