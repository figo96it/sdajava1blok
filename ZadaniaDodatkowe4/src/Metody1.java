import java.util.Random;
import java.util.Scanner;

public class Metody1 {
    public static int losowanie1(int a, int b){
        Random r=new Random();
        int x=r.nextInt(b-a)+a;
        return x;
    }

    public static double losowanie2(double a,double b){
        Random r=new Random();
        double x=r.nextDouble()*(b-a)+a;
        return x;
    }

    public static float zaokragl(double a,double b,int miejsca){
        Random r=new Random();
        double x=r.nextDouble()*(b-a)+a;

        // Jak to dziala??
        float temp=(float)(x*(Math.pow(10, miejsca)));
        temp=(Math.round(temp));
        temp=temp/(int)(Math.pow(10, miejsca));
        return temp;
    }

    public static void tablica(int[] tab,int a,int b){
        Random r=new Random();
        int c=b-a+1;
        for(int x=0;x<=c;x++,a++){
            tab[x]=a;
            System.out.println("Tab["+x+"]= "+tab[x]);
        }

    }

    public static void tablica2(double[] tab,double a,double b){
        Random r=new Random();
        double c=b-a+1;
        for(int x=0;x<=c;x++,a++){
            tab[x]=a;
            System.out.println("Tab["+x+"]= "+tab[x]);
        }

    }

    public static boolean porownanieWielkosci(double[] tab, double[] tab2){
        if(tab.length==tab2.length) return true;
        else{
            return false;
        }
    }
}
