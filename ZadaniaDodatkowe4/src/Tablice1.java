import java.util.Random;
import java.util.Scanner;

public class Tablice1 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);

        System.out.println("----------Program Wyszukiwarka!----------");
        System.out.println();
        System.out.println("Program w dowolny sposób wczytuje tablicę T o dowolnym rozmiarze.");
        System.out.println("Ile chcesz mieć elementów w tablicy?");
        int n=cin.nextInt();
        int[] tab=new int[n];
        System.out.println("Strzel czy wskazana przez ciebie liczba należy do tablicy!");
        System.out.println();
        int x=cin.nextInt();
        int suma=0;

        for(int i=0;i<n;i++){
            Random r=new Random();
            tab[i]=r.nextInt(10);
            if(tab[i]==x){
                suma=suma+1;
            }
        }

        for(int i=0;i<n;i++){
            System.out.print(tab[i]);
            System.out.print(", ");
        }
        System.out.println();

        if(suma==0){
            System.out.println("Liczba "+x+" nie powtarza się w tablicy ani razu.");
        }
        else if(suma>0){
            System.out.println("Liczba "+x+" powtarza się w tablicy "+suma+" razy!");
        }




    }
}
