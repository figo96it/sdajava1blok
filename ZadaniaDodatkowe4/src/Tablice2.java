import java.util.Random;
import java.util.Scanner;

public class Tablice2 {
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);

        System.out.println("----------Program Srednia Tablicy!----------");

        Random r=new Random();
        int n=r.nextInt(30);
        int[] tab=new int[n];
        int suma=0;

        for(int i=0;i<n;i++){
            tab[i]=r.nextInt();
            suma=suma+tab[i];
        }

        int z=0;
        for(int j=0;j<n;j++){
            z++;
            System.out.print(tab[j]);
            if(j<n-1){
                System.out.print(", ");
            }
            if(z==5){
                System.out.println();
                z=0;
            }
        }
        System.out.println();
        System.out.println();
        double srednia=suma/n;
        System.out.println("Srednia wynosi: "+srednia);

        int max=0;
        int min=0;
        for(int x=0;x<n;x++){
            if(tab[x]>srednia){
                max=max+1;
            }
            else if(tab[x]<srednia){
                min=min+1;
            }
        }

        System.out.println("Jest "+max+" elementów większych od średniej równej "+srednia);
        System.out.println("Jest "+min+" elementów mniejszych od średniej równej "+srednia);

    }
}
