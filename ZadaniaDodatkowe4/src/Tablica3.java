import java.util.Random;

public class Tablica3 {
    public static void main(String[] args) {
        System.out.println("----------Program Statystyka!----------");
        Random r = new Random();

        int[] tab = new int[20];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = r.nextInt(10)+1;
        }

        for (int i = 1; i <=10; i++) {
            int suma = 0;
            for (int j = 0; j < tab.length; j++) {
                if (tab[i] == tab[j]) {
                    suma = suma + 1;
                }
            }

            if(suma>0) {
                System.out.print("Liczba nr " + i + ": " + tab[i] + " występuje w tablicy " + suma + " razy.  ");
                System.out.println("  Jest to tylko " + ((suma / 20.0) * 100.0) + "%.");
            }
        }

    }
}
