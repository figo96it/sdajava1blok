import java.time.LocalDate;
import java.util.Scanner;

public class Birthday1 {

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        int year, month, day;

        System.out.println("Wprowadz date urodzenia: ");
        System.out.println("Dzień: ");
        day=cin.nextInt();
        System.out.println("Miesiąc: ");
        month=cin.nextInt();
        System.out.println("Rok: ");
        year=cin.nextInt();

        LocalDate birthday=LocalDate.of(year,month,day);
        LocalDate difference=LocalDate.now();
        difference=difference.minusYears(birthday.getYear());
        difference=difference.minusMonths(birthday.getMonthValue());
        difference=difference.minusDays(birthday.getDayOfMonth());

        System.out.println("Żyjesz już: ");
        System.out.println("-"+(difference.getYear())+" lat");
        System.out.println("-"+(difference.getMonthValue())+" miesięcy");
        System.out.println("-"+(difference.getDayOfMonth())+" dni");

    }
}
