import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Birthday3 {

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        System.out.println("Wprowadz datę w formacie DD/MM/YYYY HH:MM: ");
        String toParse=cin.nextLine();

        LocalDateTime birthday= LocalDateTime.parse(toParse, formatter);
        LocalDateTime difference=LocalDateTime.now();

        long diffDays= ChronoUnit.DAYS.between(birthday, difference);
        long diffMonths=ChronoUnit.MONTHS.between(birthday, difference);
        long diffYears=ChronoUnit.YEARS.between(birthday, difference);
        long diffHours=ChronoUnit.HOURS.between(birthday, difference);
        long diffMinutes=ChronoUnit.MINUTES.between(birthday, difference);
        long diffSeconds=ChronoUnit.SECONDS.between(birthday, difference);

        System.out.println("Żyjesz już: ");
        System.out.println("-"+diffYears+" lat");
        System.out.println("-"+diffMonths+" miesięcy");
        System.out.println("-"+diffDays+" dni");
        System.out.println("-"+diffHours+" godzin");
        System.out.println("-"+diffMinutes+" minut");
        System.out.println("-"+diffSeconds+" sekund");
    }
}
