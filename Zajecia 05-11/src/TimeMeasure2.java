import java.util.Locale;
import java.util.Scanner;

public class TimeMeasure2 {3r

    public static void main(String[] args) {
        long start=System.nanoTime();

        Scanner wejscie=new Scanner(System.in);
        wejscie.useLocale(Locale.US);   //zmiana systemu wprowadzania liczb dziesietnych - z kropka
        double liczba1, liczba2;
        int dzialanie;

        do {
            long started=System.nanoTime();

            System.out.print("Co chcesz zrobić? (1-DOD, 2-ODJ, 3-MNO, 4-DZI, 0-KONIEC) ");
            dzialanie = wejscie.nextInt();

            switch (dzialanie) {
                case 1:
                    System.out.print("Hej. Wprowadź pierwszą liczbę: ");
                    liczba1 = wejscie.nextDouble();
                    System.out.print("Teraz wprowadź drugą liczbę: ");
                    liczba2 = wejscie.nextDouble();
                    System.out.println("" + liczba1 + " + " + liczba2 + " = " + (liczba1 + liczba2));
                    break;
                case 2:
                    System.out.print("Hej. Wprowadź pierwszą liczbę: ");
                    liczba1 = wejscie.nextDouble();
                    System.out.print("Teraz wprowadź drugą liczbę: ");
                    liczba2 = wejscie.nextDouble();
                    System.out.println("" + liczba1 + " - " + liczba2 + " = " + (liczba1 - liczba2));
                    break;
                case 3:
                    System.out.print("Hej. Wprowadź pierwszą liczbę: ");
                    liczba1 = wejscie.nextDouble();
                    System.out.print("Teraz wprowadź drugą liczbę: ");
                    liczba2 = wejscie.nextDouble();
                    System.out.println("" + liczba1 + " * " + liczba2 + " = " + (liczba1 * liczba2));
                    break;
                case 4:
                    System.out.print("Hej. Wprowadź pierwszą liczbę: ");
                    liczba1 = wejscie.nextDouble();
                    System.out.print("Teraz wprowadź drugą liczbę: ");
                    liczba2 = wejscie.nextDouble();
                    System.out.println("" + liczba1 + " / " + liczba2 + " = " + (liczba1 / liczba2));
                    break;
                case 0:
                    System.out.println("Do widzenia!");
                    break;
                default:
                    System.out.println("Nie wybrałeś żadnego działania!");
                    break;
            }
            long stop2 = System.nanoTime();
            System.out.println("Czas wykonania pojedyńczego kroku: " + (stop2-started) + "nanosek.");
        } while (dzialanie>0);

        long stop=System.nanoTime();
        System.out.println("Czas wykonania całego programu: "+(stop-start)+"nanosek.");

    }
}
