import java.time.Duration;
import java.time.Instant;
import java.util.Locale;
import java.util.Scanner;

public class TimeMeasure {

    public static void main(String[] args) {
        Instant start=Instant.now();

        Scanner wejscie=new Scanner(System.in);
        wejscie.useLocale(Locale.US);
        double liczba1, liczba2;

        System.out.println("Hej wprowadz pierwszą liczbę: ");
        liczba1=wejscie.nextDouble();
        System.out.println("Teraz wprowadź drugą liczbę: ");
        liczba2=wejscie.nextDouble();

        System.out.println("Co chcesz zrobić? (1-DOD, 2-ODJ, 3-DZI, 4-MNO) ");
        int dzialanie=wejscie.nextInt();

        switch(dzialanie){
            case 1:
                System.out.println(""+liczba1+" + "+liczba2+" = "+(liczba1+liczba2));
                break;
            case 2:
                System.out.println(""+liczba1+" - "+liczba2+" = "+(liczba1-liczba2));
                break;
            case 3:
                System.out.println(""+liczba1+" / "+liczba2+" = "+(liczba1/liczba2));
                break;
            case 4:
                System.out.println(""+liczba1+" * "+liczba2+" = "+(liczba1*liczba2));
                break;
            default:
                System.out.println("Nie wybrałeś żadnego działania!");
                break;
        }

        Instant stop=Instant.now();
        Duration duration=Duration.between(start, stop);
        System.out.println("Czas wykonania: "+duration.getSeconds()+" sek., "+duration.getNano()+" nanosek.");
    }
}
