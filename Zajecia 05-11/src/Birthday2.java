import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Birthday2 {

    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Wprowadz date w formacie DD/MM/YYYY: ");
        String toParse=cin.nextLine();

        LocalDate birthday=LocalDate.parse(toParse,formatter);
        LocalDate difference=LocalDate.now();

        difference=difference.minusYears(birthday.getYear());
        difference=difference.minusMonths(birthday.getMonthValue());
        difference=difference.minusDays(birthday.getDayOfMonth());

        System.out.println("Żyjesz już: ");
        System.out.println("-"+(difference.getYear())+" lat");
        System.out.println("-"+(difference.getMonthValue())+" miesięcy");
        System.out.println("-"+(difference.getDayOfMonth())+" dni");
    }
}
