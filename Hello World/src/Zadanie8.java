import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner wejscie=new Scanner(System.in);
        System.out.println("Program pobiera 10 liczb od uzytkownika za pomoca petli for.");

        int liczba1,suma=0,min=0,max=0;
        for(int i=0;i<10;i++){
            System.out.println("Podaj " +(i+1)+"-ta liczbe: ");
            liczba1=wejscie.nextInt();

            suma=suma+liczba1;

            if(i==0){
                min=liczba1;
                max=liczba1;
            }
            else{
                if(liczba1>max){
                    max=liczba1;
                }
                if(liczba1<min){
                    min=liczba1;
                }
            }


        }
        System.out.println("Suma wprowadzonych liczb wynosi: " +suma);
        System.out.println("Srednia wprowadzonych liczb wynosi: " +(double)suma/10.0);
        System.out.println("Maximum wprowadzonych liczb: " + max);
        System.out.println("Minimum wprowadzonych liczb: " + min);


    }
}
