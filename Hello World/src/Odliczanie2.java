public class Odliczanie2 {
    public static void odlicz1(int ile) {
        for (int i = 1; i <ile; i++) {
            System.out.println("Instrukcja numer " + i);
        }
    }

    public static void odlicz2(int ile) {
        for (int i =ile; i > 0; i--) {
            System.out.println("Pozostalo " + i + " instrukcji!");
        }
    }

    public static void main(String[] args) {
        System.out.println("Cześć!");
        odlicz1(3);
        System.out.println("Chwila przerwy kolego...");
        odlicz2(14);
        System.out.println();

        odlicz1(5);
        odlicz1(10);
        odlicz2(3);
        odlicz2(41);
    }
}
