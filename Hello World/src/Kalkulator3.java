import javax.swing.*;
import java.util.Locale;
import java.util.Scanner;

public class Kalkulator3 {
    public static void main(String[] args) {
        Scanner wejscie=new Scanner(System.in);
        wejscie.useLocale(Locale.US); //dzieki tej funkcji useLocale mozemy uzywac kropek a nie przecinków
        double liczba1,liczba2;

        System.out.println("Wprowadz pierwszą liczbę: ");
        liczba1=wejscie.nextDouble();

        System.out.println("Wprowadz drugą liczbę: ");
        liczba2=wejscie.nextDouble();

        System.out.println("Jakie chcesz wykonac dzialanie ? (1-DODAWANIE 2-ODEJ. 3-MNOŻ. 4-DZIE.)");
        int dzialanie = wejscie.nextInt();

        if(dzialanie==1){
            System.out.println("" + liczba1 + "+" + liczba2 + "=" + (liczba1 + liczba2));
        }
        else if(dzialanie==2){
            System.out.println("" + liczba1 + "-" + liczba2 + "=" + (liczba1 - liczba2));
        }
        else if(dzialanie==3){
            System.out.println("" + liczba1 + "*" + liczba2 + "=" + (liczba1 * liczba2));
        }
        else if(dzialanie==4){
            System.out.println("" + liczba1 + "/" + liczba2 + "=" + (liczba1 / liczba2));
        }
        else System.out.println("Nastąpił błąd.");

        //PRZY wypisywaniu, zaczynając od ""(pusty łańcuch znaków) wymuszamy sklejenie napisów
        //wejscie.nextLine(); - wyczysci bufor z ewentualnych pozostałości.
        //jest to uzyteczne gdy po wprowadzeniu liczb zaczynamy wprowadzać napisy

    }
}
