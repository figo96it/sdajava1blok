import java.util.Locale;
import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner wejscie=new Scanner(System.in);
        wejscie.useLocale(Locale.US); //dzieki tej funkcji useLocale mozemy uzywac kropek a nie przecinków

        System.out.println("Program odliczający od zadanej liczby przez użytkownika do zera.");
        System.out.println("Podaj liczbę: ");
        int liczba1=wejscie.nextInt();

        int i=liczba1;
        while(liczba1>0){
            System.out.println(liczba1);
            liczba1--;
        }

        //ta sama petla tylko forem
        System.out.println("Program odliczający od zadanej liczby przez użytkownika do zera.");
        System.out.println("Podaj liczbę: ");
        int liczba2=wejscie.nextInt();

        for(int x=liczba2;liczba2>0;liczba2--){
            System.out.println(liczba2);
        }





    }
}
