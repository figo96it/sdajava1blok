import java.util.Locale;
import java.util.Scanner;

public class Kalkulator5 {
    public static void main(String[] args) {
        Scanner wejscie=new Scanner(System.in);
        wejscie.useLocale(Locale.US); //dzieki tej funkcji useLocale mozemy uzywac kropek a nie przecinków
        double liczba1,liczba2;

        System.out.println("Wprowadz pierwszą liczbę: ");
        liczba1=wejscie.nextDouble();

        System.out.println("Wprowadz drugą liczbę: ");
        liczba2=wejscie.nextDouble();

        System.out.println("Jakie chcesz wykonac dzialanie ? (1-DODAWANIE 2-ODEJ. 3-MNOŻ. 4-DZIE.)");
        int dzialanie = wejscie.nextInt();

        switch(dzialanie){
            case 1:
                System.out.println("" + liczba1 + "+" + liczba2 + "=" + (liczba1 + liczba2));
                break;
            case 2:
                System.out.println("" + liczba1 + "-" + liczba2 + "=" + (liczba1 - liczba2));
                break;
            case 3:
                System.out.println("" + liczba1 + "*" + liczba2 + "=" + (liczba1 * liczba2));
                break;
            case 4:
                System.out.println("" + liczba1 + "/" + liczba2 + "=" + (liczba1 / liczba2));
                break;
        }
        System.out.println("Czy chcesz wykonać kolejne działanie?.(1-TAK, 2-NIE)");
        dzialanie= wejscie.nextInt();

        while(dzialanie==1){
            System.out.println("Jakie chcesz wykonac dzialanie ? (1-DODAWANIE 2-ODEJ. 3-MNOŻ. 4-DZIE.)");
            dzialanie = wejscie.nextInt();

            switch(dzialanie){
                case 1:
                    System.out.println("" + liczba1 + "+" + liczba2 + "=" + (liczba1 + liczba2));
                    break;
                case 2:
                    System.out.println("" + liczba1 + "-" + liczba2 + "=" + (liczba1 - liczba2));
                    break;
                case 3:
                    System.out.println("" + liczba1 + "*" + liczba2 + "=" + (liczba1 * liczba2));
                    break;
                case 4:
                    System.out.println("" + liczba1 + "/" + liczba2 + "=" + (liczba1 / liczba2));
                    break;
                case 0:
                    System.out.println("Nie wybrales zadnego dzialania ?");
                    break;
            }

        }

    }
}