public class Zmienne {
    public static void main(String[] args) {
        int a = 3, b = 4;

        System.out.println("Dodawanie a + b: " + (a + b));
        System.out.println("Odejmowanie a - b: " + (a - b));
        System.out.println("Mnożenie a * b: " + (a * b));
        System.out.println("Dzielenie a/b: " + (a / b));
        System.out.println("Reszta dzielenia a przez b: " + (a % b));

        System.out.println("Porównanie a==b:" + (a==b));
        System.out.println("Porównanie a!=b:" + (a!=b));
        System.out.println("Porównanie a<b:" + (a<b));
        System.out.println("Porównanie a<=b:" + (a<=b));
        System.out.println("Porównanie a>b:" + (a>b));
        System.out.println("Porównanie a>=b:" + (a>=b));

        System.out.println();

        a=-5; b=-5;

        System.out.println("Dodawanie a + b: " + (a + b));
        System.out.println("Odejmowanie a - b: " + (a - b));
        System.out.println("Mnożenie a * b: " + (a * b));
        System.out.println("Dzielenie a/b: " + (a / b));
        System.out.println("Reszta dzielenia a przez b: " + (a % b));

        System.out.println("Porównanie a==b:" + (a==b));
        System.out.println("Porównanie a!=b:" + (a!=b));
        System.out.println("Porównanie a<b:" + (a<b));
        System.out.println("Porównanie a<=b:" + (a<=b));
        System.out.println("Porównanie a>b:" + (a>b));
        System.out.println("Porównanie a>=b:" + (a>=b));




    }
}
