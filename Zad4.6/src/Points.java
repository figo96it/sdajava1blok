public class Points {
    private Point[] data;
    private int counter;

    public Points(){
        data=new Point[3];
        counter=0;
    }

    public Points(int size){
        data=new Point[size];
        counter=0;
    }

    public Points(Point[] p){
        data=new Point[p.length];
        for (int i = 0; i <p.length ; i++) {
            data[i]=new Point(p[i]);
        }
        counter=data.length;
    }

    public Points(Points p){
        data=new Point[p.data.length];
        for (int i = 0; i <p.data.length ; i++) {
            data[i]=new Point(p.data[i]);
        }
        counter=data.length;
    }

    public Point getPoint(int i){
        return new Point(data[i]);
    }

    public void addPoint(Point p){
        if(counter==data.length) return;
        for(int i=0;i<data.length;i++){
            if(data[i]==null){
                counter++;
                data[i]=new Point(p);
                return;
            }
        }
    }

    public void deletePoint(Point p){
        if(p==null) return;
        for (int i = 0; i <data.length ; i++) {
            if(data[i]!=null && data[i].equals(p)){
                data[i]=null;
                counter--;
            }
        }
    }

    public void setPoint(int i,Point p){
        if(i>=data.length) return;
        if(data[i]==null) counter++;
        data[i]=new Point(p);
    }

    public String toString(){
        String result=""+counter+": ";
        for (int i = 0; i <data.length ; i++) {
            result=result+" "+data[i];
        }
        return result;
    }

    public double usedPercentage(){
        return (double)counter/(double)data.length*100;
    }

    public void changeSize(int newSize){
        if(newSize==data.length) return;

        Point[] newData= new Point[newSize];
        counter=0;

        for (int i = 0; i <Math.min(data.length, newSize) ; i++) {
            newData[i]=data[i];
            if(newData[i]!=null) counter++;
        }

        data=newData;
    }

    public static void main(String[] args) {
        Points p=new Points(5);
        System.out.println(p);

        p.setPoint(1,new Point(4,4));
        System.out.println(p);
        p.setPoint(1,new Point(2,2));
        System.out.println(p);

        p.addPoint(new Point(1,1));
        System.out.println(p);
        p.addPoint(new Point(1,-1));
        System.out.println(p);
        p.addPoint(new Point(1,-2));
        System.out.println(p);
        p.addPoint(new Point(1,1));
        System.out.println(p);

        p.deletePoint(new Point(1,1));
        System.out.println(p);

        System.out.println("Uzyto "+p.usedPercentage()+" % przestrzeni tablicy.");

        p.changeSize(7);
        System.out.println(p);
        System.out.println("Uzyto "+p.usedPercentage()+" % przestrzeni tablicy.");

        p.addPoint(new Point(1,-2));
        System.out.println(p);
        p.addPoint(new Point(1,1));
        System.out.println(p);
        p.addPoint(new Point(1,-2));
        System.out.println(p);
        p.addPoint(new Point(1,1));
        System.out.println(p);
        System.out.println("Uzyto "+p.usedPercentage()+" % przestrzeni tablicy.");

        p.changeSize(4);
        System.out.println(p);
        System.out.println("Uzyto "+p.usedPercentage()+" % przestrzeni tablicy.");
        p.addPoint(new Point(1,4));
        System.out.println(p);
    }
}
