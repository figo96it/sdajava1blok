import java.util.Scanner;

public class Zadanie2_7 {

    public static void main(String[] args) {

        Scanner cin=new Scanner(System.in);
        System.out.println("Wprowadz dane do 1 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int a=cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int b=cin.nextInt();
        double[][] macierz=new double[a][b];
        System.out.println();

        TabliceWielowymiarowe.losuj(macierz);
        TabliceWielowymiarowe.wypisz(macierz);

        System.out.println("Wprowadz dane do 2 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int c=cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int d=cin.nextInt();
        double[][] macierz2=new double[c][d];
        System.out.println();

        TabliceWielowymiarowe.losuj(macierz2);
        TabliceWielowymiarowe.wypisz(macierz2);

        if((a==c)&(b==d)){
            System.out.println();
            System.out.println("Powstała macierz3 na skutek dodawania macierzy 1 i 2.");
            System.out.println();
            double[][] macierz3=new double[a][b];
            TabliceWielowymiarowe.dodawanie(macierz,macierz2,macierz3);
        }

        if((a==d)&(b==c)){
            System.out.println();
            System.out.println("Powstała macierz4 na skutek mnożenia macierzy 1 i 2.");
            System.out.println();
            double[][] macierz4=new double[a][d];
            TabliceWielowymiarowe.mnozenie(macierz,macierz2,macierz4);
            TabliceWielowymiarowe.wypisz(macierz4);
        }
        else{
            System.out.println("Na danych wymiarach macierzy nie możemy wykonać ani dodawania ani mnożenia.!");
        }
    }
}
