import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class TabliceWielowymiarowe {

    public static void pobierz(double[][] T){
        Scanner s=new Scanner(System.in);
        s.useLocale(Locale.US);

        for(int i=0;i<T.length;i++){
            for(int j=0;j<T[i].length;j++){
                System.out.println("T["+i+"]["+j+"]= ");
                T[i][j]=s.nextDouble();
            }
        }
    }

    public static void losuj(double[][] T){
        Random r=new Random();

        for(int i=0;i<T.length;i++){
            for(int j=0;j<T[i].length;j++){
                T[i][j]=r.nextDouble()*100;
            }
        }
    }

    public static void wypisz(double[][] T){
        for(int i=0;i<T.length;i++){
            System.out.print("[");
            for(int j=0;j<T[i].length;j++){
                System.out.print(T[i][j]);
                if(j<T.length){
                    System.out.print(", ");
                }
            }
            System.out.println("]");
        }
    }

    public static double suma(double[][] T){
        double suma=0.0;

        for(int i=0;i<T.length;i++) {
            for (int j = 0; j < T[i].length; j++) {
                suma = suma + T[i][j];
            }
        }
        return suma;
    }

    public static double przekatna(double[][] T){
        double przekatna=0.0;

        for(int i=0;i<T.length;i++) {
            for (int j = 0; j < T[i].length; j++) {
                if (i == j) {
                    przekatna = przekatna + T[i][j];
                }
            }
        }
        return przekatna;
    }

    public static void dodatnie(double[][] T){
        for(int i=0;i<T.length;i++) {
            int a=0;
            for (int j = 0; j < T[i].length; j++) {
                if(T[i][j]>0){
                    a++;
                }
                if(a==3){
                    for(int x = 0; x < T.length;x++) {
                        for (int y = 0; y < T[x].length; y++) {
                            System.out.print(T[x][y]);
                            if(y<T[x].length-1){
                                System.out.print(", ");
                            }
                        }
                        System.out.println();
                    }

                }
            }
        }
    }

    public static void dodawanie(double[][] T,double[][] F, double[][] G){
        for(int i=0;i<T.length;i++){
            System.out.print("T=[");
            for(int j=0;j<T[i].length;j++){
                G[i][j]=T[i][j]+F[i][j];
                System.out.print(G[i][j]);
                if(j<T[i].length-1){
                    System.out.print(", ");
                }
            }
            System.out.println("]");
        }
    }

    public static void mnozenie(double[][] T, double[][] F, double[][] G){
        double suma=0.0;

        for(int i=0;i<G.length;i++){
            for(int j=0;j<G[i].length;j++){
                for(int x=0;x<T.length;x++){
                    suma+=T[i][x]*F[x][j];
                }
                G[i][j]=suma;
                suma=0.0;
            }
        }
    }
}
