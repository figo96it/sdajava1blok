public class Obliczenia1 {

    private int min=0;
    private int max=10;

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    private double ogranicz(double a){
        while(a<min){
            a=a+(max-min+1);
        }
        while(a>max){
            a=a-(max-min+1);
        }

        return a;
    }

    public double suma(double a, double b){
        return ogranicz(a+b);
    }

    public double srednia(double a, double b){
        return ogranicz(suma(a,b)/2.0);
    }

    public double minimum(double a,double b){
        if (a <= b) {
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }

    public double maximum(double a,double b){
        if(a>=b){
            return ogranicz(a);
        }
        else{
            return ogranicz(b);
        }
    }

    public static void main(String[] args) {
        Obliczenia1 ob1,ob2;

        ob1=new Obliczenia1();
        ob2=new Obliczenia1();

        ob2.setMin(-15);
        ob2.setMax(20);

        System.out.println(ob1.suma(12,30));
        System.out.println(ob1.suma(-8,-5));
        System.out.println(ob1.suma(8,0));
        System.out.println(ob1.suma(-7,27));

        System.out.println();

        System.out.println(ob2.suma(12,30));
        System.out.println(ob2.suma(-8,-5));
        System.out.println(ob2.suma(8,0));
        System.out.println(ob2.suma(-7,27));



    }
}
