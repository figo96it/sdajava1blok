import java.util.Scanner;

public class TShirt2 {

    public enum TShirtSizes{
        None(0), S(5), M(7), L(10), XL(12), XXL(16);

        private int inchSize;
        private TShirtSizes(int inch){
            inchSize=inch;
        }

        public int getInchSize(){
            return inchSize;
        }
    }

    public enum TShirtColours { White, Red, Green, Blue, Black };

    private TShirtSizes size;
    private TShirtColours colour;
    private String producer;
    private String model;

    public TShirt2(){
        size=TShirtSizes.M;
        colour=TShirtColours.Black;
        producer="simple";
        model="default";
    }

    public TShirt2(TShirtSizes s, TShirtColours c, String p, String m){
        size=s;
        colour=c;
        producer=p;
        model=m;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public TShirtColours getColour() {
        return colour;
    }

    public String getModel() {
        return model;
    }

    public String getProducer() {
        return producer;
    }

    public void setColour(TShirtColours colour) {
        this.colour = colour;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public String toString(){
        return ""+size+" ("+size.getInchSize()+") "+colour+" "+producer+" model: "+model;
    }

    public boolean equals(TShirt2 t2){
        if(size!=t2.size) return false;
        if(colour!=t2.colour) return false;
        if(producer!=t2.producer) return false;
        if(model!=t2.model) return false;
        return true;
    }

    //TESTOWANIE
    public static void main(String[] args) {
        TShirt2 t1=new TShirt2();
        TShirt2 t2=new TShirt2(TShirt2.TShirtSizes.XXL, TShirt2.TShirtColours.Green,"Made in USA","big");
        System.out.println(t1);
        System.out.println(t2);

        Scanner in=new Scanner(System.in);
        String size,colour, producer, model;

        System.out.println("Twoja wymarzona koszulka: ");
        System.out.print("Rozmiar: ");
        size=in.nextLine();
        System.out.println("Kolor: ");
        colour=in.nextLine();
        System.out.println("Producent: ");
        producer=in.nextLine();
        System.out.println("Model: ");
        model=in.nextLine();

        size=size.toUpperCase();
        colour=colour.substring(0,1).toUpperCase()+colour.substring(1).toLowerCase();

        Tshirt t3=new Tshirt(Tshirt.TShirtSizes.valueOf(size), Tshirt.TShirtColours.valueOf(colour), producer, model );
        System.out.println(t3);

        System.out.println("Twoja druga wymarzona koszulka: ");
        System.out.print("Rozmiar: ");
        size=in.nextLine();
        System.out.println("Kolor: ");
        colour=in.nextLine();
        System.out.println("Producent: ");
        producer=in.nextLine();
        System.out.println("Model: ");
        model=in.nextLine();

        size=size.toUpperCase();
        colour=colour.substring(0,1).toUpperCase()+colour.substring(1).toLowerCase();

        Tshirt t4=new Tshirt(Tshirt.TShirtSizes.valueOf(size), Tshirt.TShirtColours.valueOf(colour), producer, model );
        System.out.println(t4);

        System.out.println();

        if(t3.equals(t4)){
            System.out.println("To ta sama koszulka!");
        }
    }

}
