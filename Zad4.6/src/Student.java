public class Student {

    private String firstName;
    private String lastName;
    private int age;

    public static int counter=0;

    public Student(){
        firstName="";
        lastName="";
        age=0;

        counter ++;
    }

    public Student(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
        age=0;

        counter++;
    }

    public Student(String firstName, String lastName, int age){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;

        counter++;
    }

    public Student(Student s){
        firstName=s.firstName;
        lastName=s.lastName;
        age=s.age;

        counter++;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString(){
        if(firstName.equals("")){
            return "<dane studenta sa niekompletne> (niepoprawne imie)";
        }
        if(lastName.equals("")){
            return "<dane studenta sa niekompletne> (niepoprawne nazwisko)";
        }

        String result=firstName+" "+lastName;
        if(age>0){
            result=result+", lat: "+age;
        }
        else{
            result=result+" <dane studenta sa niekompletne> (niepoprawny wiek)";
        }

        return result;
    }

    public static void main(String[] args) {
        Student s1,s2,s3,s4;

        s1=new Student();
        s2=new Student("Anna","A");
        s3=new Student("Beata","B",25);

        s1.setFirstName("Lukasz");
        s1.setLastName("A.");
        s1.setAge(20);

        s4=new Student(s1);
        Student s5=new Student();

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        System.out.println(s5);
    }
}
