import java.util.Scanner;

public class Tshirt {

    public enum TShirtSizes{S, M, L, XXL, };
    public enum TShirtColours{ White, Black, Blue, Red, Green};

    public static String availableSizes(){
        String result="<";
        for(int i=0;i<TShirtSizes.values().length;i++){
            result=result+TShirtSizes.values()[i];
            if(i<TShirtSizes.values().length-1){
                result=result+", ";
            }
        }
        result=result+">";
        return result;
    }

    public static String availableColours(){
        String result="<";
        for(TShirtColours colour : TShirtColours.values()){
            result=result + colour + ", ";
        }

        result=result.substring(0,result.length()-2);
        result=result+">";
        return result;
    }

    private TShirtSizes size;
    private TShirtColours colour;
    private String producer;
    private String model;

    public Tshirt(){
        size=TShirtSizes.M;
        colour=TShirtColours.Black;
        producer="Made in China";
        model="universal";
    }

    public Tshirt(TShirtSizes size,TShirtColours colour,String producer,String model){
        this.size=size;
        this.colour=colour;
        this.producer=producer;
        this.model=model;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public TShirtColours getColour() {
        return colour;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColour(TShirtColours colour) {
        this.colour = colour;
    }

    public String toString(){
        return ""+size+" "+colour+" "+producer+" model: "+model;
    }

    public boolean equals(Tshirt t2){
        if(size!=t2.size) return false;
        if(colour!=t2.colour) return false;
        if(producer!=t2.producer) return false;
        if(model!=t2.model) return false;
        return true;
    }

    // TESTOWANIE
    public static void main(String[] args) {
        Tshirt t1=new Tshirt();
        Tshirt t2=new Tshirt(TShirtSizes.XXL,TShirtColours.Green,"Made in Usa","big");
        System.out.println(t1);
        System.out.println(t2);

        Scanner in=new Scanner(System.in);
        String size,colour,producer,model;

        System.out.println("Twoja wymarzona koszulka: ");
        System.out.print("Rozmiar "+availableSizes()+": ");
        size=in.nextLine();
        System.out.println("Kolor "+availableColours()+": ");
        colour=in.nextLine();
        System.out.println("Producent: ");
        producer=in.nextLine();
        System.out.println("Model: ");
        model=in.nextLine();

        size=size.toUpperCase();
        colour=colour.substring(0,1).toUpperCase()+colour.substring(1).toLowerCase();

        Tshirt t3=new Tshirt(TShirtSizes.valueOf(size), TShirtColours.valueOf(colour), producer, model );
        System.out.println(t3);

        System.out.println("Twoja druga wymarzona koszulka: ");
        System.out.print("Rozmiar "+availableSizes()+": ");
        size=in.nextLine();
        System.out.println("Kolor "+availableColours()+": ");
        colour=in.nextLine();
        System.out.println("Producent: ");
        producer=in.nextLine();
        System.out.println("Model: ");
        model=in.nextLine();

        size=size.toUpperCase();
        colour=colour.substring(0,1).toUpperCase()+colour.substring(1).toLowerCase();

        Tshirt t4=new Tshirt(TShirtSizes.valueOf(size), TShirtColours.valueOf(colour), producer, model );
        System.out.println(t4);

        System.out.println();

        if(t3.equals(t4)){
            System.out.println("To ta sama koszulka!");
        }

    }

}
